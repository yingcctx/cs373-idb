CityScoop

Names: Gitlab ID, EID: 

Jack Perkins: @jackp5150, jwp2844

Jacob Dittoe: @jacob.dittoe, jmd6542

Ying Su: @yingcctx, ys24487

Marlowe Johnson: @msj879, msj879

---------------------------------------

GIT SHA: 4aa05289229b45d2ec91e51de1477a8df415c24b

---------------------------------------

Phase 1 Project Leader: Ying Su

Phase 2 Project Leader: Jacob Dittoe

Phase 3 Project Leader: Jack Perkins

Phase 3 Project Leader: Marlowe Johnson

Responsibilities: Delegate tasks to the group, ensure everybody is keeping up with their work, keep group in communication.

--------------------------------------

Link to Pipelines: https://gitlab.com/yingcctx/cs373-idb/-/pipelines

Website Link: https://www.cityscoop.me
API Link: https://api.cityscoop.me

--------------------------------------

Phase 1:

Estimated & Actual Completion Times: 

Hannah Hughes | Estimated: 7, Actual: 12

Jack Perkins  | Estimated: 15, Actual: 25

Jacob Dittoe  | Estimated: 10, Actual: 20

Ying Su       | Estimated: 15, Actual: 35

Marlowe Johnson | Estimated: 10, Actual: 15



Phase 2:

Estimated & Actual Completion Times: 

Hannah Hughes | Estimated: 0, Actual: 0

Jack Perkins  | Estimated: 15, Actual: 20

Jacob Dittoe  | Estimated: 25, Actual: 30

Ying Su       | Estimated: 20, Actual: 25

Marlowe Johnson | Estimated: 10, Actual: 20



Phase 3:

Estimated & Actual Completion Times: 

Jack Perkins  | Estimated: 10, Actual: 12

Jacob Dittoe  | Estimated: 10, Actual: 15

Ying Su       | Estimated: 10, Actual: 12

Marlowe Johnson | Estimated: 5, Actual: 10


Phase 4:

Estimated & Actual Completion Times: 

Jack Perkins  | Estimated: 6, Actual: 6

Jacob Dittoe  | Estimated: 6, Actual: 6

Ying Su       | Estimated: 6, Actual: 6

Marlowe Johnson | Estimated: 6, Actual: 6