#Structure of code inspired by Austin Eats
import sys
import time
import pytest
import unittest
import os
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import Remote
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

URL = "https://www.cityscoop.me/"
PATH = "./tests/chromedriver"

local = False #Set to False when pushing to gitlab

global driver, wait

class TestNavbar(unittest.TestCase):
    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(cls):
        ops = Options()
        ops.add_argument("--headless")
        ops.add_argument("--disable-gpu")
        ops.add_argument("--window-size=1280,800")
        ops.add_argument("--allow-insecure-localhost")
        ops.add_argument("--log-level=3")
        ops.add_argument("--no-sandbox")
        ops.add_argument("--disable-dev-shm-usage")

        cls.driver = webdriver.Chrome(PATH, options=ops)
        cls.driver.get(URL)


    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def test_1_hotel(self):
        # at home page
        print("starting test_navbar_home")
        print("driver", self.driver.current_url)
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/nav/a").click()
        # print(self.driver.current_url, "AHHHHHHHHHHHH")
        self.assertEqual(self.driver.current_url, URL)
    
    def test_2_attraction_page(self):
        # attractions
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/nav/ul/li[1]/a").click()
        assert self.driver.current_url == URL + "attractions"

    def test_3_attraction_to_home(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/nav/a").click()
        assert self.driver.current_url == URL

    
    def test_4_resturants(self):
        # attractions
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/nav/ul/li[2]/a").click()
        assert self.driver.current_url == URL + "restaurants"

    def test_5_restraunt_to_home(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/nav/a").click()
        assert self.driver.current_url == URL


    def test_6_hotels(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/nav/ul/li[3]/a").click()
        assert self.driver.current_url == URL + "hotels"

    def test_7_hotel_to_home(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/nav/a").click()
        assert self.driver.current_url == URL


    def test_8_about(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/nav/ul/li[6]/a").click()
        assert self.driver.current_url == URL + "about"

    def test_9_about_to_home(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/nav/a").click()
        assert self.driver.current_url == URL

    def test_10_attraction_to_hotel(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/nav/ul/li[1]/a").click()
        assert self.driver.current_url == URL + "attractions"

        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/nav/ul/li[3]/a").click()
        assert self.driver.current_url == URL + "hotels"
        # self.driver.back()
        # self.driver.back()

    # adding filtering tests

    #can maybe update to check instance
    def test_11_hotel_filter(self):
        assert self.driver.current_url == URL + "hotels"
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/div/html/body/div/form/div[2]/div[1]/div/button").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/div/html/body/div/form/div[2]/div[1]/div/div/div/a[6]").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/div/html/body/div/form/div[3]/div/button").click()
        assert self.driver.current_url == URL + "hotels"

    def test_12_rest_filter(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/nav/ul/li[2]/a").click()
        assert self.driver.current_url == URL + "restaurants"
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/div/html/body/div/form/div[1]/div[2]/div/button").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/div/html/body/div/form/div[1]/div[2]/div/div/div/a[2]").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/div/html/body/div/form/div[3]/div/button").click()
        assert self.driver.current_url == URL + "restaurants"
        print(URL, "1")

    def test_13_rest_search(self):
        print(URL, "2")
        assert self.driver.current_url == URL + "restaurants"
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/div/html/body/section[1]/input").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/div/html/body/section[1]/input").send_keys("test")
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/div/html/body/div/form/div[3]/div/button").click()
        print(URL, "3")
        assert self.driver.current_url == URL + "restaurants"

    def test_14_hotel_search(self):
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/nav/ul/li[3]/a").click()
        assert self.driver.current_url == URL + "hotels"
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/div/html/body/section[1]/input").click()
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/div/html/body/section[1]/input").send_keys("test")
        self.driver.find_element(by=By.XPATH, value="//*[@id=\"root\"]/div/html/body/div/form/div[3]/div/button").click()
        assert self.driver.current_url == URL + "hotels"










if __name__ == "__main__":
    unittest.main()

