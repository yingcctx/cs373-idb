/**
 * @jest-environment jsdom
 */
 import React from "react";
 import { act } from "react-dom/test-utils";
 import { Card, Row, Col } from 'react-bootstrap';
 import { render, waitFor } from "@testing-library/react"
 import { screen, configure } from '@testing-library/react'
 
 import { TouristAPIData } from './pages/Models/MockData/TouristSpotData';
 import {SpotCard, createBulletList} from './pages/Models/components/SpotCard';
 import RestaurantCard from './pages/Models/components/RestaurantCard';
 import Home from "./pages/Home";
 import HotelCard from "./pages/Models/components/HotelCard";
 
 const restdata = {
   "rest_address": [
     "303 Red River St",
     "Austin, TX 78701"
   ],
   "rest_city": "Austin",
   "rest_id": 0,
   "rest_image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/Ae-cks9Ng7i4pAzVHSmGWw/o.jpg",
   "rest_latlng": [
     30.26386,
     -97.73796
   ],
   "rest_name": "Moonshine Patio Bar & Grill",
   "rest_pricing": "$$",
   "rest_rating": 4.5,
   "rest_review_count": 5643,
   "rest_transactions": [
     "delivery"
   ],
   "rest_type": [
     {
       "alias": "southern",
       "title": "Southern"
     },
     {
       "alias": "breakfast_brunch",
       "title": "Breakfast & Brunch"
     },
     {
       "alias": "cocktailbars",
       "title": "Cocktail Bars"
     }
   ],
   "rest_website": "https://www.yelp.com/biz/moonshine-patio-bar-and-grill-austin?adjust_creative=m-V1ZRRCB2LClSJvIcO_HQ&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=m-V1ZRRCB2LClSJvIcO_HQ"
 }

 const hoteldata = {
    "hot_S2C": "4*",
    "hot_address": "San Jacinto At 8Th St",
    "hot_category": "5EST",
    "hot_city": "AUSTIN",
    "hot_desc": "This outstanding property is a magnificently appointed, luxurious hotel that surrounds guests with comfort and style. It is located just 17 kilometres from Austin Bergstrom International Airport and features a prime situation in the heart of the Texas State Capital. Inside, visitors will find thoughtfully appointed accommodations with spectacular views. Outside, is Texas at its best. With the heart of the thriving downtown business centre at traveller's doorstep, they will be just steps away from the Austin Convention Centre and the Texas State Capitol. For pleasure, the lively 6th Street Entertainment District is located nearby. Quite simply, the property provides the ultimate in luxury, comfort and convenience. Mini Fridge & microwave available by request only.  A valid credit card must be secured for incidentals at check-in.  Cash is not accepted at check in but can be used to settle the balance at check-out.",
    "hot_email": "No Email Listed",
    "hot_id": 1,
    "hot_image": "01/016134/016134a_hb_w_059.jpg",
    "hot_lat": 30.269003,
    "hot_lon": -97.740429,
    "hot_name": "Omni Austin Hotel Downtown",
    "hot_phone": "+0015124541144",
    "hot_website": "No Website Listed"
 }

 const attdata = {
    "att_id": 0,
    "att_img": "https://scontent-hou1-1.xx.fbcdn.net/v/t1.6435-9/39142954_1776483195774446_8190386274218016768_n.jpg?stp=dst-jpg_p640x640&_nc_cat=102&ccb=1-7&_nc_sid=e3f864&_nc_ohc=bhVR7NNVQuYAX_q0JJj&_nc_ht=scontent-hou1-1.xx&oh=00_AfB3nu3S6mdA0EJ9fbjEoVVD2EXmoTunOrGXTRkC9v1Cmg&oe=642F54EC",
    "att_name":"Alamo Drafthouse Cinema - The Ritz",
    "att_state": "Texas",
    "att_city": "Austin",
    "att_county": "Travis County",
    "att_address": "320 East 6th Street, Austin, TX 78701, United States of America",
    "att_categories": [
      "building",
      "building.entertainment",
      "building.historic",
      "entertainment",
      "entertainment.cinema",
      "tourism",
      "tourism.sights"],
    "att_num_categories": 7,
    "att_lon":-97.73958582383372,
    "att_lat":30.26740345,
    "att_phone": "+1-512-861-7020",
    "att_website":"https://drafthouse.com/austin",
    "att_hours": "None",
    "att_hots": [],
    "att_rests":[]
 }
 
 describe("spot card component test", () => {
   let listFn = createBulletList(TouristAPIData[0]);
   let testListString =
     "<ul><li>cultural</li><li>museums</li><li>interesting places</li><li>other museums</li></ul>";

 
   it("test createBulletList fn", async () => {
     expect(listFn).toEqual(testListString);
   });
 },
 
 //restaurant card test 5 total: Written by: Ashley Su
 describe("Restcard page test", () => {
   //test 1 
   it("test restaurant title", async () => {
     render(<RestaurantCard instanceNumber={0} cardData={restdata} />);
     const findTitle = await waitFor(() => screen.findByTestId("rest-title-0"));
 
     expect(findTitle.textContent).toEqual("Moonshine Patio Bar & Grill");
 
   });
   //test 2
   it("test restaurant data: num reviews", async () => {
 
     render(<RestaurantCard instanceNumber={0} cardData={restdata} />);
     const findTitle = await waitFor(() => screen.findByTestId("rest-reviews-0"));
     expect(findTitle.textContent).toEqual("Num Reviews: 5643");
 
   });
 
    //test 3
    it("test restaurant data: city ", async () => {
 
     render(<RestaurantCard instanceNumber={0} cardData={restdata} />);
     const findTitle = await waitFor(() => screen.findByTestId("rest-city-0"));
     expect(findTitle.textContent).toEqual("City: Austin");
 
   });
   //test 4
   it("test restaurant data: rating ", async () => {
 
     render(<RestaurantCard instanceNumber={0} cardData={restdata} />);
     const findTitle = await waitFor(() => screen.findByTestId("rest-rating-0"));
     expect(findTitle.textContent).toEqual("Rating: 4.5");
 
   });
   //test 5
   it("test restaurant data: price ", async () => {
 
     render(<RestaurantCard instanceNumber={0} cardData={restdata} />);
     const findTitle = await waitFor(() => screen.findByTestId("rest-price-0"));
     expect(findTitle.textContent).toEqual("Price: $$");
 
   });
 }, 

 //hotel card test 5 total: Written by: Jack Perkins & Ashley Su
describe("hotcard page test", () => {
  //test 1 
  it("test hotel title", async () => {
    render(<HotelCard instanceNumber={0} cardData={hoteldata} />);
    const findTitle = await waitFor(() => screen.findByTestId("hotel-title-0"));
    expect(findTitle.textContent).toEqual("Omni Austin Hotel Downtown");

  });
  //test 2
  it("test hotel data: city", async () => {

    render(<HotelCard instanceNumber={0} cardData={hoteldata} />);
    const findTitle = await waitFor(() => screen.findByTestId("hotel-city-0"));
    expect(findTitle.textContent).toEqual("City: AUSTIN");

  });

   //test 3
   it("test hotel data: phone num ", async () => {

    render(<HotelCard instanceNumber={0} cardData={hoteldata} />);
    const findTitle = await waitFor(() => screen.findByTestId("hotel-website-0"));
    expect(findTitle.textContent).toEqual("Website: No Website Listed");

  });
  //test 4
  it("test hotel data: S2C ", async () => {

    render(<HotelCard instanceNumber={0} cardData={hoteldata} />);
    const findTitle = await waitFor(() => screen.findByTestId("hotel-S2C-0"));
    expect(findTitle.textContent).toEqual("Health and Safety Rating: 4*");

  });
  //test 5
  it("test hotel data: rating ", async () => {

    render(<HotelCard instanceNumber={0} cardData={hoteldata} />);
    const findTitle = await waitFor(() => screen.findByTestId("hotel-rating-0"));
    expect(findTitle.textContent).toEqual("Hotel Star Rating: 5EST");

});
},

describe("Spot page test", () => {
  //test 1 
  it("test spot name", async () => {
    render(<SpotCard instanceNumber={0} cardData={attdata} />);
    const findTitle = await waitFor(() => screen.findByTestId("spot-title-0"));
    expect(findTitle.textContent).toEqual("Alamo Drafthouse Cinema - The Ritz");

  });
  //test 2
  it("test spot data: city", async () => {

    render(<SpotCard instanceNumber={0} cardData={attdata} />);
    const findTitle = await waitFor(() => screen.findByTestId("spot-city-0"));
    expect(findTitle.textContent).toEqual("City: Austin");

  });

   //test 3
   it("test spot data: phone num ", async () => {

    render(<SpotCard instanceNumber={0} cardData={attdata} />);
    const findTitle = await waitFor(() => screen.findByTestId("spot-state-0"));
    expect(findTitle.textContent).toEqual("State: Texas");

  });
  //test 4
  it("test spot data: state ", async () => {

    render(<SpotCard instanceNumber={0} cardData={attdata} />);
    const findTitle = await waitFor(() => screen.findByTestId("spot-address-0"));
    expect(findTitle.textContent).toEqual("Address: 320 East 6th Street, Austin, TX 78701, United States of America");

  });
  //test 5
  it("test spot data: address ", async () => {

    render(<SpotCard instanceNumber={0} cardData={attdata} />);
    const findTitle = await waitFor(() => screen.findByTestId("spot-lat_lon-0"));
    expect(findTitle.textContent).toEqual("Latitude and Longitude: 30.26740345, -97.73958582383372");

});
}
 ))));
