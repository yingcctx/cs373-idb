import Navbar from "./Navbar"
import Model1 from "./pages/Models/Model1"
import Model2 from "./pages/Models/Model2"
import Model3 from "./pages/Models/Model3"
import Spot from "./pages/Models/Spot"
import Home from "./pages/Home"
import About from "./pages/About"
import { Route, Routes } from "react-router-dom"
import Restaurant from "./pages/Models/Restaurant"
import Visualizations from "./pages/Visualizations"
import ProviderVisualizations from "./pages/ProviderVisualizations"
import Hotel from "./pages/Models/Hotel"
import Search from "./pages/Models/Search"
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  
  
  return (
    <>
      <Navbar />
      <div className="container_nav">

        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/attractions" element={<Model1 />} />
          <Route path="/restaurants" element={<Model2 />} />
          <Route path="/hotels" element={<Model3 />} />
          <Route path="/attractions/spot/:id" element={<Spot />} />
          <Route path="/restaurants/restaurant/:id/" element={<Restaurant />} />
          <Route path="/hotels/hotel/:id" element={<Hotel />} />
          <Route path="/about" element={<About />} />
          <Route path="/visualizations" element={<Visualizations />} />
          <Route path="providerVisualizations" element={<ProviderVisualizations/>}/>
          <Route path="/hotels/search/:query" element={<Search />} />
         
        </Routes>
      </div>
    </>
  )
}

export default App
