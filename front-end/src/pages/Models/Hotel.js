// Skeleton for displaying info on the instance page. Fills in info using method calls that will retrieve the actual data.
import "./Models.css";
import { useLocation } from "react-router-dom";
import { Link } from "react-router-dom"
import styled from 'styled-components'
import { useState, useEffect } from "react";
import { Card, Container, Button} from 'react-bootstrap';
import axios from "axios";
import { FindAllNear } from "./components/findNear";

export default function Hotel() {

const location = useLocation()
const id = location.pathname
const id2 = id.split("/")[3]

console.log(id2)

const LinksWrapper = styled.div`
display: flex;
justify-content: center;
margin: 20px
`;

const baseUrl = `https://api.cityscoop.me/hotels/id=${id2}`;
const [rest, restSet] = useState([]);
const [lat, latSet] = useState([]);
const [lon, lonSet] = useState([]);
const [addr, addrSet] = useState([]);
const [city, citySet] = useState([]);
const [image, imageSet] = useState([]);
const [name, nameSet] = useState([]);

 
useEffect(() => {
  axios.get(baseUrl).then((response) => {
    let rest = response.data
    restSet(rest);
    latSet(rest.hot_lat ? rest.hot_lat : "None")
    lonSet(rest.hot_lon ? rest.hot_lon : "None")
    addrSet(rest.hot_address ? rest.hot_address : "None")
    imageSet(rest.hot_image ? rest.hot_image : "None")
    citySet(rest.hot_city ? rest.hot_city : "None")
    nameSet(rest.hot_name ? rest.hot_name : "None")
  });
}, [baseUrl]);
if(!rest) return null;
  let pic =  "http://photos.hotelbeds.com/giata/bigger/" + image
  console.log('latlng', lat, lon)
  var relatedRestaurants = FindAllNear(lat, lon,'restaurants');
  var relatedAttractions = FindAllNear(lat, lon,'attractions');

  return (
    
    <Container >
    <Card
    bg={'dark'}
    text={'white'}
    style={{ width: '80rem', height: '180rem' }}>
    <Card.Img variant="top" style={{height:`40rem`}} src={pic + ""} />
    <Card.Text  style={{fontSize:'25px'}}>{"Find us on the map!"}</Card.Text>
    <div style={{padding: "10px"}}></div>
    <div  style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        }}>
<iframe
      width="600"
      height="450"
      loading="lazy"
      title="googlemaps"
      allowFullScreen
      referrerPolicy="no-referrer-when-downgrade"
      src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyDvaZSadN8H4Z-mtGCK-5BuwtmXFYGk268&q=${lat} ${lon}`}
    ></iframe>

     
    </div>
    <Card.Body>
      <Card.Title style={{fontSize:'90px'}}>{name}</Card.Title> 
      <Card.Subtitle style={{fontSize:'30px'}}>{<p class="font-italic">{"Address: " + addr}</p>}</Card.Subtitle>
      <Card.Text  style={{fontSize:'25px'}}>{"City: " +  city}</Card.Text>
      <Card.Text  style={{fontSize:'25px'}}>{"Contact: " +  rest.hot_phone}</Card.Text>
      <Card.Text  style={{fontSize:'25px'}}>{"About: " +  rest.hot_desc}</Card.Text>

      <Link to={rest.hot_website + ""} target="_blank">
      <Button variant="primary" style={{fontSize:'30px'}}>Learn More</Button>
       </Link>
       <div style={{padding: "30px"}}></div>
       <Card.Title style={{fontSize:'30px'}}> Related Information</Card.Title>
       <Card.Text style={{fontSize:'20px'}}>{"Restaurants nearby: "}</Card.Text>
      <LinksWrapper>
      <Link to={`https://www.cityscoop.me/restaurants/restaurant/${relatedRestaurants[0]}`} >{ relatedRestaurants[1]} Distance: {relatedRestaurants[2]}</Link>
      <div style={{padding: "10px"}}></div>
      <Link to={`https://www.cityscoop.me/restaurants/restaurant/${relatedRestaurants[3]}`} >{relatedRestaurants[4]}  Distance: {relatedRestaurants[5]}</Link>
    </LinksWrapper>
      <Card.Text style={{fontSize:'20px'}}>{"Attractions nearby: "}</Card.Text>
      <LinksWrapper>
      <Link to={`https://www.cityscoop.me/attractions/spot/${relatedAttractions[0]}`} > {relatedAttractions[1]} Distance: {relatedAttractions[2]}</Link>
      <div style={{padding: "10px"}}></div>
      <Link to={`https://www.cityscoop.me/attractions/spot/${relatedAttractions[3]}`} > {relatedAttractions[4]} Distance: {relatedAttractions[5]}</Link>
    </LinksWrapper>
    </Card.Body>
  </Card>
  </Container>
  )
}
