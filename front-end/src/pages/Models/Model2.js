import RestaurantCard from "./components/RestaurantCard";
import FavoriteList from "./components/FavoriteList";
import axios from "axios";
import { useEffect, useState, useRef } from "react";
import Pagination from "./Pagination";

import "./Models.css";
import RangeSlider from "./RangeSlider";
import FilterDropdown from "./FilterDropdown";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export default function Model2() {
  const searchQuery = useRef("");

  const baseUrl = "https://api.cityscoop.me/restaurants";
  const localUrl = "http://127.0.0.1:5000/restaurants";
  const localPageNumber = localStorage.getItem("pn");
  const localFilters = JSON.parse(localStorage.getItem("rest_filter"));

  const [favorites, setFavorites] = useState([]);
  const [rest, restSet] = useState([]);
  const [totalPosts, setTotalPosts] = useState(100);
  const sortOptions = [
    { name: "Sort", value: "sort" },
    { name: "Prices", value: "rest_pricing" },
    { name: "Rating", value: "rest_rating" },
    { name: "Reviews", value: "rest_review_count" },
  ];

  const [currFilters, currFiltersSet] = useState(localFilters || []);
  const [city, setCity] = useState("City");
  const [rating, setRating] = useState([1, 5]);
  const [numRev, setNumRev] = useState([0, 20000]);
  const [sort, setSort] = useState("sort");
  const [ascending, setAscending] = useState(false);
  const [price, setPrice] = useState("Price");
  const [flavor, setFlavor] = useState("Flavor");
  const [query, setQuery] = useState("");

  let [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(10);

  const handleSortFilter = (value) => {
    setSort(value.toLowerCase().replace(" ", "_"));
  };
  const handleOrderFilter = (value) => {
    setAscending(value === "Ascending");
  };

  const handleCityFilter = (value) => {
    if (value === "Washington, D.C.") {
      setCity("Washington, DC");
    } else {
      setCity(value.substring(0, value.indexOf(",")));
    }
  };
  const handleRatingFilter = (value) => {
    setRating(value);
  };
  const handleNumRevFilter = (value) => {
    setNumRev(value);
  };
  const handlePriceFilter = (value) => {
    setPrice(value);
  };
  const handleFlavorFilter = (value) => {
    setFlavor(value);
  };

  function getPaginatedData(number) {
    if (localPageNumber) setCurrentPage(() => (currentPage = localPageNumber));
    if (!localPageNumber) setCurrentPage(() => (currentPage = number));
  }
  function filterCheck(filterName, value) {
    let check = ["Order", "Price", "City", "Flavor", "Sort"];
    if (!check.includes(value)) {
      return { name: filterName, value: value };
    }
  }
  function onSubmit() {
    const newFilters = [];
    let filters = [
      { name: "city", value: city },
      {
        name: "sort",
        value: sortOptions.find((option) => option.name.toLowerCase() === sort)
          .value,
      },
      { name: "ascending", value: ascending },
      { name: "prices", value: price },
      { name: "flavor", value: flavor },
    ];
    for (const filter of filters) {
      let addedFilter = filterCheck(filter.name, filter.value);
      if (addedFilter) newFilters.push(addedFilter);
    }
    currFiltersSet(newFilters);
    localStorage.setItem("rest_filter", JSON.stringify(newFilters));
    localStorage.setItem("pn", 1);
  }

  const handleSearch = () => {
    const searchValue = searchQuery.current.value;
    onSubmit(); // Call onSubmit before setting the query
    setQuery(searchValue);
  };

  function intToFloat(num) {
    return num + "." + Array(1 + 1).join("0");
  }

  useEffect(() => {
    const useLocal = false;
    let url = useLocal ? localUrl : baseUrl;
    // Add query to the paginatedUrl
    let paginatedUrl = `${url}?page=${
      localPageNumber || currentPage
    }&perPage=${postsPerPage}&minRating=${intToFloat(
      rating[0]
    )}&maxRating=${intToFloat(rating[1])}&minReviews=${numRev[0]}&maxReviews=${
      numRev[1]
    }&search=${query}`;
    let urlFilters = `${paginatedUrl}`;
    urlFilters = currFilters
      .map((filter) => `&${filter.name}=${filter.value}`)
      .join("");
    if (urlFilters) paginatedUrl = paginatedUrl + urlFilters;
    axios.get(paginatedUrl).then((response) => {
      setTotalPosts(response.data.Metadata);
      restSet(response.data.Result);
    });
  }, [
    currentPage,
    currFilters,
    query,
    numRev,
    postsPerPage,
    rating,
    localPageNumber,
  ]); // Add query to the dependency array
  if (!rest) return null;

  //change page
  const paginate = async (pageNumber) => {
    await getPaginatedData(pageNumber);
    localStorage.setItem("pn", pageNumber);
    await setCurrentPage(pageNumber);
  };

  return (
    <html lang="en">
      <body className="model1_body">
        <p>Favorite List of Restaurants: </p>
        <FavoriteList
          removeFav={(favorite) => {
            setFavorites(
              favorites.splice(
                favorites.findIndex((f) => f === favorite),
                1
              )
            );
          }}
          favorites={favorites}
        />

        <section className="model1_container ">
          <Form.Control
            ref={searchQuery}
            style={{ width: "20vw" }}
            type="search"
            name="query"
            placeholder="Search restaurants"
            className="me-2"
            aria-label="Search"
          />

          <Button variant="outline-secondary" onClick={handleSearch}>
            Search
          </Button>
        </section>

        <div>
          <Form className="filter-form">
            <Row className="mx-auto text-center w-50 mb-4">
              <Col>
                {" "}
                <FilterDropdown
                  title="Sort"
                  items={sortOptions.map((option) => option.name)}
                  onChange={handleSortFilter}
                />
              </Col>
              <Col>
                <FilterDropdown
                  title="Order"
                  items={["Ascending", "Descending"]}
                  onChange={handleOrderFilter}
                />
              </Col>
              <Col>
                <FilterDropdown
                  title="Price"
                  items={["Price", "$$", "$$$", "$$$$"]}
                  onChange={handlePriceFilter}
                />
              </Col>
              <Col>
                {" "}
                <FilterDropdown
                  title="City"
                  items={[
                    "City",
                    "New York, NY",
                    "Los Angeles, CA",
                    "Dallas, TX",
                    "Austin, TX",
                    "Chicago, IL",
                    "Seattle, WA",
                    "Houston, TX",
                    "Detroit, MI",
                    "Washington, D.C.",
                    "Miami, FL",
                  ]}
                  scroll
                  onChange={handleCityFilter}
                />
              </Col>

              <Col>
                {" "}
                <FilterDropdown
                  title="Flavor"
                  items={[
                    "Flavor",
                    "Italian",
                    "Mexican",
                    "Seafood",
                    "Chinese",
                    "Cuban",
                    "Mediterranean",
                    "Southern",
                    "Vietnamese",
                    "American (Traditional)",
                    "American (New)",
                    "Pizza",
                    "Korean",
                    "French",
                    "Barbeque",
                    "Polish",
                    "Szechuan",
                    "Taiwanese",
                    "Modern European",
                    "Persian/Iranian",
                    "Japanese",
                    "Sushi",
                    "Buffet",
                    "Bars",
                    "Tapas/Small Plates",
                    "Dim Sum",
                  ]}
                  scroll
                  onChange={handleFlavorFilter}
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Label>Rating</Form.Label>
                <RangeSlider min={1} max={5} onChange={handleRatingFilter} />
              </Col>
              <Col>
                <Form.Label>Number of Reviews</Form.Label>
                <RangeSlider
                  min={0}
                  max={20000}
                  onChange={handleNumRevFilter}
                />
              </Col>
            </Row>
            <Row className="mx-auto text-center my-4">
              <Col>
                <Button variant="outline-secondary" style={{marginRight: 2 + 'em'}} onClick={() => onSubmit()}>
                  Submit
                </Button>
                <Button 
                  variant="outline-secondary"
                  onClick={() => {
                    localStorage.removeItem("rest_filter");
                    localStorage.removeItem("pn");
                    currFiltersSet((f)=>f = [])
                    setCurrentPage((p)=>p=1)
                  }}
                >
                  Reset
                </Button>
              </Col>
            </Row>
          </Form>
        </div>
        <section className="model1_container ">
          {rest.map((spot, key) => {
            return (
              <div key={key} style={{ padding: "10px" }}>
                <RestaurantCard
                  key={key}
                  cardData={spot}
                  instanceNumber={spot.rest_id}
                  addFavorite={(favorite) => {
                    if (favorites.find((fav) => fav === favorite)) return;
                    setFavorites([...favorites, favorite]);
                  }}
                  query={query} // Add this line
                ></RestaurantCard>
              </div>
            );
          })}
        </section>

        <section className="model1_container ">
          <Pagination
            postsPerPage={postsPerPage}
            totalPosts={totalPosts}
            paginate={paginate}
          ></Pagination>
        </section>
        <p>
          Total Instances: {totalPosts} Total pages:{" "}
          {Math.ceil(totalPosts / 10)}
        </p>
      </body>
    </html>
  );
}