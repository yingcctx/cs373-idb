// Skeleton for displaying info on the instance page. Fills in info using method calls that will retrieve the actual data.
import "./Models.css";
import { useLocation } from "react-router-dom";
import { Card, Container, Button  } from 'react-bootstrap';
import { useState, useEffect } from "react";
import { Link } from "react-router-dom"
import styled from "styled-components"
import axios from "axios";
import { FindAllNear } from "./components/findNear";


export default function Spot() {

  

  const LinksWrapper = styled.div`display: flex; justify-content: center; margin: 20px`;
  const location = useLocation()
  const id = location.pathname
  const id2 = id.split("/")[3]
  console.log(id2)
  let baseUrl = `https://api.cityscoop.me/attractions/id=${id2}`;
  const localUrl = `http://localhost:5000/attractions/id=${id2}`;
  baseUrl = (true) ? baseUrl : localUrl
  console.log(baseUrl)
  
  const [att, attSet] = useState([]);
  const [latlng, latlngSet] = useState([]);

  const [addrSet] = useState([]);
  const [typeSet] = useState([]);
  const [imageSet] = useState([]);

  useEffect(() => { 
    
    axios.get(baseUrl).then((response) => {
      let att = response.data
      attSet(att);
      latlngSet([response.data.att_lat, response.data.att_lon] ? [response.data.att_lat, response.data.att_lon] : "None")
      addrSet(response.data.att_address ? response.data.att_address : "None")
      imageSet(response.data.att_img ? response.data.att_img : "None")
      typeSet(response.data.rest_type[0].title ? response.data.rest_type[0].title : "None")
    });
  }, [location,id,id2,baseUrl, addrSet, imageSet, typeSet]);
  if(!att) return null;
  console.log('latlong',att)
  var relatedHotels = FindAllNear(latlng[0],latlng[1], 'hotels')
  var relatedAttractions = FindAllNear(latlng[0],latlng[1], 'attractions')

  
  return (
    
    <Container>
        <Card bg={'dark'} text={'white'} style={{ width: '80rem', height: '150rem' }}>
        <Card.Img variant="top" style={{height:`40rem`}} src={att.att_img} />
        <Card.Text  style={{fontSize:'25px'}}>{"Find us on the map!"}</Card.Text>
        <div style={{padding: "10px"}}></div>
        <div  style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}>
    <iframe
          width="600"
          height="450"
          loading="lazy"
          title="googlemaps" 
          allowFullScreen
          referrerPolicy="no-referrer-when-downgrade"
          src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyDvaZSadN8H4Z-mtGCK-5BuwtmXFYGk268&q=${latlng[0]} ${latlng[1]}`}
        ></iframe>    </div>
        <Card.Body>
          <Card.Title style={{fontSize:'90px'}}>{att.att_name}</Card.Title> 
          
          <Card.Text  style={{fontSize:'25px'}}>{"City: " + att.att_city}</Card.Text>
          <Card.Text  style={{fontSize:'25px'}}>{"Address: " + att.att_address}</Card.Text>
          <Card.Text  style={{fontSize:'25px'}}>{"Type of Establishment:" + att.att_categories} </Card.Text>
          <Card.Text  style={{fontSize:'25px'}}>{"What to do: "}</Card.Text>
          <Link to={`https://www.youtube.com/results?search_query=` + att.att_name} target="_blank" >
          <Button variant="danger" style={{fontSize:'30px'}}>Search on Youtube</Button>
          </Link>
          <Card.Text  style={{fontSize:'25px'}}>{"Hours Avaliable: "+ att.att_hours}</Card.Text>
          <Link to={att.att_website} target="_blank">
          <Button variant="primary" style={{fontSize:'30px'}}>Visit Their Site</Button>
           </Link>
           <div style={{padding: "10px"}}></div>
          <Link to={att.att_website} target="_blank">
          <Button variant="primary" style={{fontSize:'30px'}}>Learn More On Wikipedia</Button>
           </Link>
           <div style={{padding: "30px"}}></div>
           <Card.Title style={{fontSize:'30px'}}>{"Restaurants nearby"}</Card.Title>
         <LinksWrapper>
           <Link to={`https://www.cityscoop.me/restaurants/restaurant/${relatedAttractions[0]}`} >{ relatedAttractions[1]} Distance: {relatedAttractions[2]}</Link>
           <div style={{padding: "10px"}}></div>
           <Link to={`https://www.cityscoop.me/restaurants/restaurant/${relatedAttractions[3]}`} >{relatedAttractions[4]}  Distance: {relatedAttractions[5]}</Link>
         </LinksWrapper>
           <Card.Text style={{fontSize:'20px'}}>{"Other attractions nearby: "}</Card.Text>
           <LinksWrapper>
           <Link to={`https://www.cityscoop.me/hotels/hotel/${relatedHotels[0]}`} > {relatedHotels[1]} Distance: {relatedHotels[2]}</Link>
           <div style={{padding: "10px"}}></div>
           <Link to={`https://www.cityscoop.me/hotels/hotel/${relatedHotels[3]}`} > {relatedHotels[4]} Distance: {relatedHotels[5]}</Link>
         </LinksWrapper>
        </Card.Body>
      </Card>
      </Container>
      )
    
}