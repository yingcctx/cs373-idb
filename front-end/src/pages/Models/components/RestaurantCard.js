import React from 'react';
import { MockRestaurantData, YelpApiData } from '../MockData/RestaurantData';
import { Card, Row, Col } from 'react-bootstrap';
import { FavoriteButton } from './FavoriteButton'; 
import './highlight.css';

function highlightQuery(text, query) {
  const regex = new RegExp(`(${query})`, 'gi');
  const highlightedText = text.replace(regex, '<span class="highlight">$1</span>');
  return { __html: highlightedText };
}


function RestaurantCard(props) { 
  const instanceNumber = props.instanceNumber;
  const rest = props.cardData;
  var query = props.query;

  if (typeof rest.rest_image_url === 'undefined') {
    rest.rest_image_url =
      'https://www.firstbenefits.org/wp-content/uploads/2017/10/placeholder.png';
  }

  return (
    <Row>
      <Col md="auto">
        <Card bg={'dark'} text={'white'} style={{ width: '20rem', height: '40rem' }}>
          <Card.Img
            variant="top"
            style={{ width: '20rem', height: '15rem' }}
            src={rest.rest_image_url + ''}
          />
          <Card.Body>
            <Card.Title
              data-testid={`rest-title-${instanceNumber}`}
              style={{ fontSize: `30px` }}
              dangerouslySetInnerHTML={highlightQuery(rest.rest_name, query)}
            />
            <Card.Text
              data-testid={`rest-reviews-${instanceNumber}`}
              dangerouslySetInnerHTML={highlightQuery('Num Reviews: ' + rest.rest_review_count, query)}
            />
            <Card.Text dangerouslySetInnerHTML={highlightQuery('Flavor: ' + rest.rest_type, query)} />
            <Card.Text
              data-testid={`rest-city-${instanceNumber}`}
              dangerouslySetInnerHTML={highlightQuery('City: ' + rest.rest_city, query)}
            />
            <Card.Text
              data-testid={`rest-price-${instanceNumber}`}
              dangerouslySetInnerHTML={highlightQuery('Price: ' + rest.rest_pricing, query)}
            />
            <Card.Text
              data-testid={`rest-rating-${instanceNumber}`}
              dangerouslySetInnerHTML={highlightQuery('Rating: ' + rest.rest_rating, query)}
            />
            <FavoriteButton
              addFavorite={() =>
                props.addFavorite({ cardName: rest.rest_name, cardLink: `restaurant/${props.instanceNumber}` })
              }
              cardName={rest.rest_name}
              cardLink={`restaurant/${props.instanceNumber}`}
            />
            <div style={{ padding: '10px' }}></div>
            <a href={`https://www.cityscoop.me/restaurants/restaurant/${props.instanceNumber}`} {...props}>
              More Info
            </a>
          </Card.Body>

        </Card>
      </Col>
    </Row>
  );
}

export function getCardData(id) {
  return MockRestaurantData[id - 1];
}

export function getCardData2(id) {
  return YelpApiData[id - 1];
}

export default RestaurantCard;
