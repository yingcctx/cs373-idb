
import { useState, useEffect } from "react";
import axios, {} from "axios";
export function FindAllNear(lat1, long1, type){
  let short = {'restaurants': 'rest', 'hotels': 'hot','attractions': 'att'}  
  const baseUrl = `https://api.cityscoop.me/${type}`;
  const [rest, restSet] = useState([]);
  useEffect(() => {
    axios.get(baseUrl).then((response) => {
      restSet(response.data.Result);
    });
    // eslint-disable-next-line
  }, []);
  if (!rest) return null;
  var allPlacesClose = []
  var ifNoPlaces = []
  rest.forEach((spot, index) => {
    var lat2, long2;
      if(type === "restaurants") {
        lat2 = spot['rest_latlng'][0]
        long2 = spot['rest_latlng'][1]
      } else {
        lat2 = spot[short[type]+'_lat']
        long2 = spot[short[type]+'_lon']
      }
      var distanceinkm = getDistanceFromLatLonInKm(lat1,long1,lat2,long2) 
        ifNoPlaces.push(spot[short[type]+'_id'])
      //16km is about 10 miles
      if(index === 0 || index === 1){
        ifNoPlaces.push(spot[short[type]+'_name'])
        ifNoPlaces.push(Number(distanceinkm).toFixed(2) +' km')
      }
      if(distanceinkm <= 16 && distanceinkm > 0){
        allPlacesClose.push(spot[short[type]+'_id'])
        allPlacesClose.push(spot[short[type]+'_name'])
        allPlacesClose.push(distanceinkm.toFixed(2)+'km')
    }
  })
  let result = allPlacesClose.length === 0 ? ifNoPlaces : allPlacesClose
  return result
}

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1);  // deg2rad below
  var dLon = deg2rad(lon2 - lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2);

  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));  
  var d = R * c; // Distance in km
  
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}