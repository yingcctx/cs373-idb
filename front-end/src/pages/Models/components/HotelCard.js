import React from 'react';
import { MockHotelData, HotelApiData } from '../MockData/HotelData';
import { Card, Row, Col } from 'react-bootstrap';
import { FavoriteButton } from './FavoriteButton';
import './highlight.css';

function highlightQuery(text, query) {
  const regex = new RegExp(`(${query})`, 'gi');
  const highlightedText = text.replace(regex, '<span class="highlight">$1</span>');
  return { __html: highlightedText };
}


function HotelCard(props) {
  const instanceNumber = props.instanceNumber;
  const rest = props.cardData;
  var query = props.query;
  let image = 'http://photos.hotelbeds.com/giata/bigger/' + rest.hot_image; 

  return (
    <Row>
      <Col md="auto">
        <Card bg={'dark'} text={'white'} style={{ width: '20rem', height: '41rem' }}>
          <Card.Img variant="top" style={{ width: '20rem', height: '10rem' }} src={image + ''} />
          <Card.Body>
            <Card.Title
              data-testid={`hotel-title-${instanceNumber}`}
              style={{ fontSize: `30px` }}
              dangerouslySetInnerHTML={highlightQuery(rest.hot_name, query)}
            />
            <Card.Text dangerouslySetInnerHTML={highlightQuery('Address: ' + rest.hot_address, query)} />
            <Card.Text
              data-testid={`hotel-city-${instanceNumber}`}
              dangerouslySetInnerHTML={highlightQuery('City: ' + rest.hot_city, query)}
            />
            <Card.Text
              data-testid={`hotel-website-${instanceNumber}`}
              dangerouslySetInnerHTML={highlightQuery('Website: ' + rest.hot_website, query)}
            />
            <Card.Text
              data-testid={`hotel-S2C-${instanceNumber}`}
              dangerouslySetInnerHTML={highlightQuery('Health and Safety Rating: ' + rest.hot_S2C, query)}
            />
            <Card.Text
              data-testid={`hotel-rating-${instanceNumber}`}
              dangerouslySetInnerHTML={highlightQuery('Hotel Star Rating: ' + rest.hot_category, query)}
            />
            <FavoriteButton
              addFavorite={() =>
                props.addFavorite({ cardName: rest.hot_name, cardLink: `hotels/${props.instanceNumber}` })
              }
              cardName={rest.rest_name}
              cardLink={`restaurant/${props.instanceNumber}`}
            />
            <div style={{ padding: '10px' }}></div>
            <a href={`https://www.cityscoop.me/hotels/hotel/${props.instanceNumber}`} {...props}>
              More Info
            </a>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

export function getCardData(id) {
  return MockHotelData[id - 1];
}

export function getCardData2(id) {
  return HotelApiData[id - 1];
}

export default HotelCard;
