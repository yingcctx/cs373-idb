

import React from 'react'

export const FavoriteButton = (props) => {
  return (
    <button onClick={()=>props.addFavorite({cardName: props.cardName, cardLink: props.cardLink})}>Add to favorites</button>
  )
}
