import React from 'react';
import { TouristAPIData } from '../MockData/TouristSpotData';
import { FavoriteButton } from './FavoriteButton';
import { Card, Row, Col, Badge } from 'react-bootstrap';
import './highlight.css';

function highlightQuery(text, query) {
  const regex = new RegExp(`(${query})`, 'gi');
  const highlightedText = text.replace(regex, '<span class="highlight">$1</span>');
  return { __html: highlightedText };
}


export function SpotCard({ cardData, instanceNumber, query, addFavorite }) {
  const att = cardData;

  return (
    <Row>
      <Col md="auto">
        <Card bg={'dark'} text={'white'} style={{ width: '20rem', height: '52rem' }}>
          <Card.Img src={att.att_img + ''} data-testid={`img-src-${att.att_id}`} variant="top" style={{ width: '20rem', height: '15rem' }} />
          <Card.Body>
            <Badge bg="primary">{att.att_city}</Badge>{''}
            <Badge f="success">{att.att_categories[0]}</Badge>{''}
            <Badge s="warning">{att.att_categories[1]}</Badge>{''}
            <Badge t="info">{att.att_categories[2]}</Badge>{''}
            <Card.Title
              data-testid={`spot-title-${att.att_id}`}
              style={{ fontSize: `30px` }}
              dangerouslySetInnerHTML={highlightQuery(att.att_name, query)}
            />
            <Card.Subtitle data-testid={`spot-address-${instanceNumber}`} dangerouslySetInnerHTML={highlightQuery("Address: " + att.att_address, query)} />
            <Card.Text data-testid={`spot-about-${instanceNumber}`} dangerouslySetInnerHTML={highlightQuery("About: ", query)} />
            <Card.Text data-testid={`spot-state-${instanceNumber}`} dangerouslySetInnerHTML={highlightQuery("State: " + att.att_state, query)} />
            <Card.Text data-testid={`spot-city-${instanceNumber}`} dangerouslySetInnerHTML={highlightQuery("City: " + att.att_city, query)} />
            <Card.Text dangerouslySetInnerHTML={highlightQuery("Hours Open: " + att.att_hours, query)} />
            <Card.Text data-testid={`spot-lat_lon-${instanceNumber}`} dangerouslySetInnerHTML={highlightQuery("Latitude and Longitude: " + att.att_lat + ", " + att.att_lon, query)} />
            <Card.Text dangerouslySetInnerHTML={highlightQuery("Type of Establishment: " + att.att_categories[0], query)} />
            <FavoriteButton
              addFavorite={() => addFavorite({ cardName: att.att_name, cardLink: `spot/${instanceNumber}` })}
              cardName={att.att_name}
              cardLink={`attraction/${instanceNumber}`}
            />
            <div style={{ padding: '10px' }}></div>

            <a href={`https://www.cityscoop.me/attractions/spot/${instanceNumber}`}>
              More Info
            </a>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}


export function getCardData2(id) {
  return TouristAPIData[id - 1];
}

export function createBulletList(data) {
  var kindsArray = data.kinds.split(', ');
  let curHtml = '<ul>';
  kindsArray.forEach((el) => {
    curHtml += `<li>${el.trim()}</li>`;
  });
  curHtml += '</ul>';
  return curHtml;
}

export default SpotCard;