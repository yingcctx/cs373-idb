

//import { useState } from 'react';
import { Link } from 'react-router-dom';

export default function FavoriteList(props){
  return (
    <ul style={{'display': 'flex', justifyContent: 'center'}}>
        {
        props.favorites.map((spot, key) => {
            return (
              <li
              key={key} 
              style={{'list-style': 'none', 'paddingRight': '10px'}}>
               <Link to={spot.cardLink}>{spot.cardName}
               </Link>
               {/* <button onClick={() => {props.removeFav(spot.cardName) 
                }}>x</button> */}
               </li>
              )
            })
        }
    </ul>
  )
}

  