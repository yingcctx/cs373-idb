import React, { useState, useEffect } from 'react';
import HotelCard from './components/HotelCard';
import RestaurantCard from './components/RestaurantCard';
import SpotCard from './components/SpotCard';
import { useParams } from 'react-router-dom';
import axios from "axios";
import Pagination from './Pagination';

export default function Search() {
  const [hotels, setHotels] = useState([]);
  const [spot, setSpot] = useState([]);
  const [rest, setRest] = useState([]);

  const [totalPosts, setTotalPosts] = useState(10);
  const { query } = useParams();
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(10);
  const [searchQuery, setSearchQuery] = useState(query)
  
  // Add this state for search input
  const [searchInput, setSearchInput] = useState(query);

  useEffect(() => {
    fetchHotels();
    fetchRest();
    fetchSpot();
    // eslint-disable-next-line
  }, [searchQuery]);
  

  const handleSearchSubmit = (e) => {
    e.preventDefault();
    setSearchQuery(searchInput);
  };  

//   stuff for hotels
  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = hotels.slice(indexOfFirstPost, indexOfLastPost);
  
//   restaurants
  const indexOfLastPost2 = currentPage * postsPerPage;
  const indexOfFirstPost2 = indexOfLastPost2 - postsPerPage;
  const currentPosts2 = rest.slice(indexOfFirstPost2, indexOfLastPost2);

  const indexOfLastPost3 = currentPage * postsPerPage;
  const indexOfFirstPost3 = indexOfLastPost3 - postsPerPage;
  const currentPosts3 = spot.slice(indexOfFirstPost3, indexOfLastPost3);

  const paginate = async (pageNumber) => {
    await getPaginatedData(pageNumber);
    await setCurrentPage(pageNumber);
  };

  const fetchHotels = async (searchTerm = searchQuery) => {
    try {
      axios.get(`https://api.cityscoop.me/hotels?search=${searchTerm}`).then((response) => {
        setTotalPosts(response.data.Metadata);
        setHotels(response.data.Result);
      });
    } catch (error) {
      console.error('Error fetching hotels:', error);
    }
  };

  const fetchRest = async (searchTerm = searchQuery) => {
    try {
      axios.get(`https://api.cityscoop.me/restaurants?search=${searchTerm}`).then((response) => {
        setTotalPosts(response.data.Metadata);
        setRest(response.data.Result);
      });
    } catch (error) {
      console.error('Error fetching restaurants:', error);
    }
  };

  const fetchSpot = async (searchTerm = searchQuery) => {
    try {
      axios.get(`https://api.cityscoop.me/attractions?search=${searchTerm}`).then((response) => {
        setTotalPosts(response.data.Metadata);
        setSpot(response.data.Result);
      });
    } catch (error) {
      console.error('Error fetching attractions:', error); 
    }
  };

  function getPaginatedData(number) { 
    setCurrentPage(number);
  }

 return (
    <div>
      <section>
        <form onSubmit={handleSearchSubmit}>
          <input
            type="text"
            value={searchInput}
            onChange={(e) => setSearchInput(e.target.value)}
            placeholder="Search..."
          />
          <button type="submit">Search</button>
        </form>
      </section>
      <section className="model1_container ">
        {/* In Search component */}
        {currentPosts.map((spot, key) => (
        <div key={key} style={{ padding: '10px' }}>
            <HotelCard key={key} cardData={spot} instanceNumber={spot.hot_id} query={searchQuery} />
        </div>
        ))}
        {currentPosts2.map((spot, key) => (
        <div key={key} style={{ padding: '10px' }}>
            <RestaurantCard key={key} cardData={spot} instanceNumber={spot.rest_id} query={searchQuery} />
        </div>
        ))}
        {currentPosts3.map((spot, key) => (
        <div key={key} style={{ padding: '10px' }}>
            <SpotCard key={key} cardData={spot} instanceNumber={spot.att_id} query={searchQuery} />
        </div>
        ))}

        {hotels.length === 0 && rest.length === 0 && spot.length === 0 && (
          <div style={{ padding: '10px' }}>
            <h3>No results found for '{searchQuery}'</h3>
          </div>
        )}
      </section>
      <section className="model1_container ">
        <Pagination postsPerPage={postsPerPage} totalPosts={totalPosts} paginate={paginate} />
      </section>
    </div>
  );
}


