import HotelCard from './components/HotelCard'
import FavoriteList from './components/FavoriteList';
import axios from 'axios';
import { useEffect, useState, useRef } from 'react';
import Pagination from './Pagination';

import "./Models.css";
import FilterDropdown from './FilterDropdown';
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// hotel model

export default function Model2() {
  const searchQuery = useRef("");


  const baseUrl = 'https://api.cityscoop.me/hotels';
  const localUrl = "http://127.0.0.1:5000/hotels"
  const localPageNumber = localStorage.getItem("pn");
  const localFilters = JSON.parse(localStorage.getItem("rest_filter"));


  const [favorites, setFavorites] = useState([]);
  const [rest, restSet] = useState([]);

  // change sortable
  // want city, hotel style, health and safety, star rating, 
  // const[currFilters, currFiltersSet] = useState([]);
  const [currFilters, currFiltersSet] = useState(localFilters||[])
  const [city, setCity] = useState("City");
  const [rating, setRating] = useState("Rating");
  const [health, setHealth] = useState("Health");
  const [sort, setSort] = useState("sort");
  const [ascending, setAscending] = useState(false);
  const [hasWebsite, setWebsite] = useState("Has Website");
  const [totalPosts, setTotalPosts] = useState(100);
  const[query, setQuery] = useState("");


  // don't change yet
  let [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(10)


  const handleSortFilter = (value) => {
    if(value === "Health Rating") {
      value = "hot_S2C"
    } else if (value === "Star Rating") {
      value = "hot_category"
    }
    setSort(value);
  };
  const handleOrderFilter = (value) => {
    if(value === "Ascending") {
      value = "true"
    }
    setAscending(value);
  };

  

  const handleCityFilter = (value) => {
    if (value === "Washington, D.C.") {
      setCity("Washington DC");
    } else {
      setCity(value.substring(0, value.indexOf(",")));
    }
  };
  const handleRatingFilter = (value) => {
    setRating(value);
    
  };
  const handleHealthFilter = (value) => {

    setHealth(value);
    
  };
  const handleWebsiteFilter = (value) => {
    setWebsite(value);
    
  };


  const handleSearch = () => {
    const searchValue = searchQuery.current.value;
    onSubmit(); // Call onSubmit before setting the query
    setQuery(searchValue);
  };

  function getPaginatedData(number) {
    if(localPageNumber)  setCurrentPage(() => currentPage = localPageNumber);
    if(!localPageNumber) setCurrentPage(() => currentPage = number);

  }
  function filterCheck(filterName, value){
    let check = ['Order', 'City', 'Has Website', 'Rating', 'Health']
    if(!check.includes(value)) return {name: filterName, value: value}
  }
  function onSubmit(){
    const newFilters = [];
    let filters =  [
    {name: "city", value: city},
    {name: "sort" ,value: sort},
    {name: "ascending", value: ascending},
    {name: "hasWebsite",value: hasWebsite},
    {name: "starRating",value: rating},
    {name: "healthRating",value: health},
    ]

    for (const filter of filters) {
      let addedFilter = filterCheck(filter.name, filter.value);
      if(addedFilter) newFilters.push(addedFilter);
    }
    currFiltersSet(newFilters);
    localStorage.setItem("rest_filter", JSON.stringify(newFilters));
    localStorage.setItem("pn", 1);
    }







    useEffect(() => {
      const useLocal = false;
      let url = useLocal ? localUrl : baseUrl;

      let paginatedUrl = `${url}?page=${
        localPageNumber || currentPage
      }&perPage=${postsPerPage}
      &search=${query}`;
      let urlFilters = `${paginatedUrl}`
     
      urlFilters = currFilters.map(filter => `&${filter.name}=${filter.value}`).join('')
      if(urlFilters) paginatedUrl = paginatedUrl + urlFilters
      console.log(paginatedUrl)
      axios.get(paginatedUrl).then((response) => {
        setTotalPosts(response.data.Metadata)
        restSet(response.data.Result);
      });
      // eslint-disable-next-line
    }, [currentPage, currFilters, query]);
    if (!rest) return null;
  
    //change page
    const paginate = async (pageNumber) => {
      await getPaginatedData(pageNumber);
    localStorage.setItem("pn", pageNumber);
    await setCurrentPage(pageNumber);
    };



  return (

    <html lang="en">

<body className="model1_body">
<p>Favorite List of Hotels: </p>
        <FavoriteList removeFav={(favorite) => {
          setFavorites(favorites.splice(favorites.findIndex((f) => f === favorite), 1))
        }}
          favorites={favorites} />
        

    <section className="model1_container ">
    <Form.Control
              ref={searchQuery}
              style={{ width: "20vw" }}
              type="search"
              name="query"
              placeholder="Search hotels"
              className="me-2"
              aria-label="Search"
            />

        <Button variant="outline-secondary" onClick={handleSearch}>
          Search
        </Button>

        </section>

        <div>



          <Form className="filter-form">
        <Row className="mx-auto text-center w-50 mb-4">
          <Col>
            {" "}
            <FilterDropdown
              title="Sort"
              items={[
                "Sort",
                "Health Rating",
                "Star Rating",
              ]}
              onChange={handleSortFilter}
            />
          </Col>
          <Col>
            <FilterDropdown
              title="Order"
              items={["Ascending", "Descending"]}
              onChange={handleOrderFilter}
            />
          </Col>
          <Col>
            {" "}
            <FilterDropdown
              title="City"
              items={[
                "City",
                "New York, NY",
                "Dallas, TX",
                "Austin, TX",
                "Chicago, IL",
                "Seattle, WA",
                "Houston, TX",
                "Detroit, MI",
                "Washington, D.C.",
                "Miami, FL",
               
              ]}
              scroll
              onChange={handleCityFilter}
            />
          </Col>

          <Col>
            {" "}
            <FilterDropdown
              title="Has Website"
              items={["Has Website","True"]}
              onChange={handleWebsiteFilter}
            />
          </Col>

     
        </Row>
        <Row>
        <Col>
            <FilterDropdown
              title="Star Rating"
              items={["Rating","1EST", "2EST", "3EST", "4EST", "5EST"]}
              onChange={handleRatingFilter}
            />
          </Col>

          <Col>
            <FilterDropdown
              title="Health Rating"
              items={["Rating","1*", "2*", "3*", "4*"]}
              onChange={handleHealthFilter}
            />
          </Col>
          
        
        </Row>
        <Row className="mx-auto text-center my-4">
              <Col>
                <Button variant="outline-secondary" style={{marginRight: 2 + 'em'}} onClick={() => onSubmit()}>
                  Submit
                </Button>
                <Button 
                  variant="outline-secondary"
                  onClick={() => {
                    localStorage.removeItem("rest_filter");
                    localStorage.removeItem("pn");
                    currFiltersSet((f)=>f = [])
                    setCurrentPage((p)=>p=1)
                  }}
                >
                  Reset
                </Button>
              </Col>
            </Row>

      </Form>


<section className="model1_container ">

      {
        rest.map((spot, key) => {
              return (
                <div key={key} style={{ padding: '10px' }}>
                  <HotelCard key={key} cardData={spot} instanceNumber={spot.hot_id} addFavorite={(favorite) => {
                    if (favorites.find((fav) => fav === favorite)) return;
                    setFavorites([...favorites, favorite]);
                  }}
                  query={query}>
                  </HotelCard>
                </div>

              )
            })
          }

    </section>


</div>

        <section className="model1_container ">
        <Pagination postsPerPage={postsPerPage}
         totalPosts={totalPosts}
        paginate={paginate} ></Pagination>
        </section>
        <p>Total Instances: {totalPosts} Total pages: {Math.ceil(totalPosts/10)}</p> 

      </body>
    </html> 
  )
}