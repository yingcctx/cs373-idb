

// import { useState } from 'react';

import SpotCard from './components/SpotCard'
//import { TouristAPIData } from "./MockData/TouristSpotData";
import "./Models.css";
import FavoriteList from './components/FavoriteList';
import axios from 'axios';
import { useEffect, useState, useRef } from 'react';
import Pagination from './Pagination';
//import PaginationImport from '@mui/material/Pagination';
import FilterDropdown from './FilterDropdown';
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export default function Model1() {
  // const [favorites, setFavorites] = useState([])
  const searchQuery = useRef("");
  const baseUrl = 'https://api.cityscoop.me/attractions';
  const localUrl = 'http://localhost:5000/attractions';
  const localPageNumber = localStorage.getItem("pn");
  const localFilters = JSON.parse(localStorage.getItem("att_filter"));
  
  //baseUrl = (true) ? baseUrl : localUrl

  const [favorites, setFavorites] = useState([]);
  const [att, attSet] = useState([]);
  var [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(10)
  const [attFilterSet] = useState([]);
  
  const[totalPosts, setTotalPosts] = useState(100);
  const[currFilters, currFiltersSet] = useState(localFilters || []);
  const[city,setCity] = useState("City");
  const[state,setState] = useState("State");
  const[sort,setSort] = useState("sort");
  const[ascending,setAscending] = useState(false);
  const[query,setQuery] = useState("");
  const[hasHours,setHasHours] = useState("has Hours");  

  const handleSortFilter = (value) => {
    if(value === "State") {
      value = "att_state"
    } else if (value === "City") {
      value = "att_city"
    } else if (value === "Num Categories") {
      value = "att_num_categories"
    }
    setSort(value);
  };
  const handleOrderFilter = (value) => {
    setAscending(value === "Ascending");
  };

  const handleCityFilter = (value) => {
    setCity(value)
  };

  const handleStateFilter = (value) => {
    setState(value);
  }

  const handleHasHoursFilter = (value) => {
    setHasHours(value);
  }
  function getPaginatedData(number){

    if (localPageNumber) setCurrentPage(() => (currentPage = localPageNumber));
    if (!localPageNumber) setCurrentPage(() => (currentPage = number));
  }

  function filterCheck(filterName,value){
    let check = ['Order',"State","City","HasHours"]
    if(!check.includes(value)){
      return {name: filterName, value:value}
    }
  }

  function onSubmit(){
    const newFilters = [];
    let filters = [
      {name:"city", value:city},
      {name: "sort" ,value:sort},
      {name: "ascending", value:ascending},
      {name: "state", value:state},
      {name: "hasHours", value:hasHours},
    ]
    for (const filter of filters) {
      let addedFilter = filterCheck(filter.name, filter.value);
      console.log(addedFilter)
      if(addedFilter) newFilters.push(addedFilter)
    }
    
    currFiltersSet(newFilters)
    localStorage.setItem("att_filter", JSON.stringify(newFilters));
    localStorage.setItem("pn", 1);
  }

  const handleSearch = () => {
    const searchValue = searchQuery.current.value;
    onSubmit(); // Call onSubmit before setting the query
    setQuery(searchValue);
  };
  
  useEffect(() => {
    const useLocal = false;
    let url = useLocal ? localUrl : baseUrl;
  
    // Add query to the paginatedUrl
    let paginatedUrl = `${url}?page=${localPageNumber||currentPage}&perPage=${postsPerPage}&search=${query}`;
    let urlFilters = `${paginatedUrl}`
    urlFilters = currFilters.map(filter => `&${filter.name}=${filter.value}`).join('')
    if(urlFilters) paginatedUrl = paginatedUrl + urlFilters
    console.log(paginatedUrl)
    axios.get(paginatedUrl).then((response) => {
      setTotalPosts(response.data.Metadata)
      attSet(response.data.Result);
      attFilterSet(response.data.Result);
    });
  }, [currentPage, currFilters, query, postsPerPage, city, state, attFilterSet, hasHours,localPageNumber]); // Add query to the dependency array
  if (!att) return null;

  const paginate = async (pageNumber) => {
    await getPaginatedData(pageNumber);
    localStorage.setItem("pn", pageNumber);
    await setCurrentPage(pageNumber);
  };

  return (
    <html lang="en">
      <body class="model1_body">
      <p>Favorite List of Attractions: </p>
        <FavoriteList 
        removeFav={(favorite) => {
          setFavorites(
            favorites.splice
            (favorites.findIndex((f) => f === favorite), 
            1))
        }}
          favorites={favorites} />
        
        
        <section className="model1_container ">
        <Form.Control
            ref={searchQuery}
            style={{ width: "20vw" }}
            type="search"
            name="query"
            placeholder="Search attractions"
            className="me-2"
            aria-label="Search"
          />
           <Button variant="outline-secondary" onClick={handleSearch}>
            Search
          </Button>
        </section>

          <div>
            <Form className="filter-form">
              <Row className="mx-auto text-center w-50 mb-4">
                <Col>
                  {" "}
                  <FilterDropdown
                    title="Sort"
                    items={["Sort", "State", "City", "Num Categories"]}
                    onChange={handleSortFilter}
                  />
                </Col>
                <Col>
                  <FilterDropdown
                    title="Order"
                    items={["Ascending", "Descending"]}
                    onChange={handleOrderFilter}
                  />
                </Col>
                <Col>
                  <FilterDropdown
                    title="State"
                    items={[
                      "State", 
                      "Texas", 
                      "California", 
                      "Illinois", 
                      "New York",
                      "Washington",
                      "Michigan",
                      "Florida",
                      "District of Columbia"]}
                    onChange={handleStateFilter}
                  />
                </Col>
                <Col>
                  {" "}
                  <FilterDropdown
                    title="City"
                    items={[
                      "City",
                      "New York",
                      "Los Angeles",
                      "Dallas",
                      "Austin",
                      "Chicago",
                      "Seattle",
                      "Houston",
                      "Detroit",
                      "Washington DC",
                      "Miami",
                    ]}
                    scroll
                    onChange={handleCityFilter}
                  />
                </Col>

                <Col>
                  {" "}
                  <FilterDropdown
                    title="Has Hours"
                    items={["Has Hours","True"]}
                    onChange={handleHasHoursFilter}
                  />
                </Col>
              </Row>
              <Row className="mx-auto text-center my-4">
                <Col>
                  <Button
                    variant="outline-secondary"
                    onClick={() => onSubmit()}
                  >
                    Submit
                  </Button>
                  <Button 
                  variant="outline-secondary"
                  onClick={() => {
                    localStorage.removeItem("att_filter");
                    localStorage.removeItem("pn");
                    currFiltersSet((f)=>f = [])
                    setCurrentPage((p)=>p=1)
                  }}
                >
                  Reset
                </Button>
                </Col>
              </Row>
            </Form>
          </div>
          <section className="model1_container">
          {att.map((spot, key) => {
            return (
              <div key={key} style={{ padding: "10px" }}>
                <SpotCard
                  key={key}
                  cardData={spot}
                  instanceNumber={spot.att_id}
                  addFavorite={(favorite) => {
                    if (favorites.find((fav) => fav === favorite)) return;
                    setFavorites([...favorites, favorite]);
                  }}
                  query={query} // Add this line
                ></SpotCard>
              </div>
            );
          })}
        </section>
        <section className="model1_container ">
          <Pagination 
          postsPerPage={postsPerPage} 
          totalPosts={totalPosts} 
          paginate={paginate}>
          </Pagination>
        </section>
        <p>Total Instances: {totalPosts} Total pages: {Math.ceil(totalPosts/10)}</p>

      </body>
    </html>
  )
}
