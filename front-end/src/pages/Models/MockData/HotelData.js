// Make api calls in here
// Exports this data into actual cards/when you click on card

// import React, { useState, useCallback } from 'react';
// import { Link } from 'react-router-dom';
// import axios from "axios";


// export function HotelData() {

//     var axios = require('axios');

//     var config = {
//       method: 'get',
//     maxBodyLength: Infinity,
//       url: 'https://api.test.hotelbeds.com/hotel-content-api/1.0/hotels?fields=all&language=ENG&from=1&countryCode=US&useSecondaryLanguage=false&to=3',
//       headers: { 
//         'Api-key': 'd5cfa9543adbbe2c886f40f0c1cc1455', 
//         'X-Signature': '69597d0e0dd165158512fe807380061f8eca4e02a0f1c0709c404767099db7ba', 
//         'Accept': 'application/json', 
//         'Accept-Encoding': 'gzip', 
//         'appid': 'd5cfa9543adbbe2c886f40f0c1cc1455'
//       }
//     };
    
//     axios(config)
//     .then(function (response) {
//       console.log(JSON.stringify(response.data));
//     })
//     .catch(function (error) {
//       console.log(error);
//     });

//     const [hotelName, setHotelName] = useState(0);
//     const [hotelAddress, setHotelAddress] = useState([]);

//     const hotel = {
//         name: "",
//         summary: "",
//         address: "",
//         hours: "",
//         price: "",
//         ratings: "",
//         comment: "",
//         type: "",
//         image: "",
//         image2: ".",
//         longsummary: "",
//         hotels: "",
//         restaurants: "",
//         spots: "",
//         todo: "",
//         phone: "",
//         link: "",
//         id: 1
//     };

//     const [hotelData, setHotelData] = useState([]);

//     const getHotelData = useCallback(async () => {
//         const response = await axios(config);
//         console.log(response)
//         setHotelData(response.data);
//     }, []);

    // React.useEffect(() => {
    //     getCommits();
    //     getIssues()
    // }, []);

    // const getIssues = async () => {
    //     await axios.get("https://gitlab.com/api/v4/projects/43263137/issues").then((response) => {
    //         setIssues(response.data)
    //     })
    // }

// }

export const HotelApiData = [

    {
        id:1,
        "code": 13040,
        "name": {
            "content": "Comfort Suites Airport - Austin"
        },
        "description": {
            "content": "All suite hotel ideally located minutes from the 6th Street downtown entertainment district."
        },
        "coordinates": {
            "longitude": -97.6935526728494,
            "latitude": 30.2165278271597
        },
        "categoryCode": "3EST",
        "address": {
            "content": "7501 E. Ben White Blvd.",
            "street": "7501 E. Ben White Blvd."
        },
        "city": {
            "content": "AUSTIN"
        },
        "email": "gm.TX573@choicehotels.com",
        "phones": [
            {
                "phoneNumber": "5123232121",
                "phoneType": "PHONEBOOKING"
            },
            {
                "phoneNumber": "512-386-6000",
                "phoneType": "PHONEHOTEL"
            }
        ],
        "web": "https://www.choicehotels.com/texas/austin/comfort-suites-hotels/tx573"
    },
    {
        id:2,
        "code": 16134,
        "name": {
            "content": "Omni Austin Hotel Downtown"
        },
        "description": {
            "content": "This outstanding property is a magnificently appointed, luxurious hotel that surrounds guests with comfort and style. It is located just 17 kilometres from Austin Bergstrom International Airport and features a prime situation in the heart of the Texas State Capital. Inside, visitors will find thoughtfully appointed accommodations with spectacular views. Outside, is Texas at its best. With the heart of the thriving downtown business centre at traveller's doorstep, they will be just steps away from the Austin Convention Centre and the Texas State Capitol. For pleasure, the lively 6th Street Entertainment District is located nearby. Quite simply, the property provides the ultimate in luxury, comfort and convenience. Mini Fridge & microwave available by request only.  A valid credit card must be secured for incidentals at check-in.  Cash is not accepted at check in but can be used to settle the balance at check-out."
        },
        "coordinates": {
            "longitude": -97.740429,
            "latitude": 30.269003
        },
        "categoryCode": "5EST",
        "address": {
            "content": "San Jacinto At 8Th St",
            "street": "San Jacinto At 8Th St"
        },
        "city": {
            "content": "AUSTIN"
        },
        "phones": [
            {
                "phoneNumber": "+0015124541144",
                "phoneType": "PHONEBOOKING"
            },
            {
                "phoneNumber": "+15124763700",
                "phoneType": "PHONEHOTEL"
            },
            {
                "phoneNumber": "+15123974888",
                "phoneType": "FAXNUMBER"
            }
        ],
        "S2C": "4*"
    },
    {
        id:3,
        "code": 40201,
        "name": {
            "content": "The Line Austin"
        },
        "description": {
            "content": "Brand new hotel in the heart of Austin\n\nLocated at the crossroads of Downtown and Town Lake, the LINE Austin embodies the natural beauty and creative spirit of Austin, with direct views and easy access to the city's beloved hike and bike trails. It is centrally located within walking distance of the Red River music district, Rainey Street, Convention Center, 2nd Street shopping, South Congress and Austin's Eastside neighborhood.\n\nThe hotel opens with Arlo Grey, the debut lakeside restaurant helmed by Top Chef 10 winner Kristen Kish. In room dining is also available with menu by Kristen Kish. The ground floor is also home to the first Austin outpost of popular LA-based coffee shop Alfred. Other hotel amenities include an infinity pool overlooking the lake, expansive lobby and bar area for working or socializing, onsite retail store with sundries and souvenirs, complimentary bike rentals to explore Austin, wellness program with local fitness vendors including paddle-boarding and boat rentals, as well as an in-house guest experience team to create custom Austin experiences.\n\nIn room amenities include complimentary high-speed Wi-Fi, floor to ceiling windows with sweeping lake and city views, minimalist décor with custom furnishings, artwork by local Austin artists, fully stocked minibar, American Medicinal Arts bath amenities, as well as turn down service on request. Suite bookings receive upgraded in room amenity package.\n\n\nWe love pets – pets stay free."
        },
        "coordinates": {
            "longitude": -97.743972,
            "latitude": 30.262795
        },
        "categoryCode": "4EST",
        "address": {
            "content": "111 East Cesar Chavez St.",
            "street": "111 East Cesar Chavez St."
        },
        "city": {
            "content": "AUSTIN"
        },
        "email": "reservations.atx@thelinehotel.com",
        "phones": [
            {
                "phoneNumber": "+5124789611",
                "phoneType": "PHONEBOOKING"
            },
            {
                "phoneNumber": "+5124789611",
                "phoneType": "PHONEHOTEL"
            },
            {
                "phoneNumber": "+5124731502",
                "phoneType": "FAXNUMBER"
            }
        ],
        "web": "www.thelinehotel.com/austin",
        "S2C": "4*"
    }
]





export const MockHotelData = [
{
    name: "LaQuinta",
    summary: "Located off I-35, La Quinta Inn® by Wyndham Austin Capitol/Downtown gives you access to the “Live Music Capital of the World.” While you're here, settle in with our fitness center, outdoor pool, and free daily breakfast.",
    address: "300 E 11th St, Austin, TX 78701",
    hours: "24/7",
    price: "89 a night",
    ratings: "3.2/5",
    comment: "The Best of Austin Enjoy restaurants and breweries on Sixth Street or take a boat tour",
    type: "2 star",
    image: "./model_images/ComfortSuites1.png",
    image2: "./components/model_images/ComfortSuites2.png",
    longsummary: "Located across the street from the Texas Capitol building, this 4-story hotel is also 1 mile from the University of Texas at Austin. The colorful rooms and suites include pillow-top mattresses, free WiFi and flat-screen TVs with HD channels, plus desks and coffeemakers. Upgraded rooms add minifridges and microwaves, while suites include whirlpool tubs or full kitchens. Kids 18 and under stay free with a parent or grandparent. Free continental breakfast is offered in addition to meeting facilities, an outdoor pool and a fitness center.",
    hotels: "Moxy",
    restaurants: "Acre41",
    spots: "Texas Capitol",
    todo: "Free Wi-Fi,    Paid parking,    Accessible,    Outdoor pool,    Air-conditioned,    Business center",
    phone: "(512) 476-1166",
    link: "https://www.wyndhamhotels.com/laquinta/austin-texas/la-quinta-inn-austin-capitol-downtown/overview",
    id: 1
},
{
    name: "Moxy (Austin)",
    summary: "Set in the lively West Campus neighborhood, this relaxed hotel is a 5-minute walk from the University of Texas at Austin, 1 mile from I-35 and 2 miles from the Austin Convention Center.",
    address: "2552 Guadalupe St, Austin, TX 78705",
    hours: "24/7",
    price: "138 a night",
    ratings: "4.1/5",
    comment: "The Best of Austin Enjoy restaurants and breweries on Sixth Street or take a boat tour",
    type: "Western style hotel",
    image: "./model_images/omni1.png",
    image2: "./components/model_images/omni2.png",
    longsummary: " At Moxy Hotels, we don’t take ourselves too seriously. But we’re seriously into showing you a good time with small but smart rooms, stylish communal spaces and bars you’ll love. The Western-inspired rooms feature flat-screen TVs and Wi-Fi. A hip bar offers a terrace. Other amenities include a laid-back fast food restaurant and a fitness center. Breakfast and parking are available.",
    hotels: "La Quinta",
    restaurants: "Acre41",
    spots: "Texas Capitol",
    todo: "Free Wi-Fi,    Paid breakfast,    Paid parking,    Accessible,    Air-conditioned,    Business center",
    phone: "(737) 471-2621",
    link: "https://moxy-hotels.marriott.com/",
    id: 2
},
{
    name: "Carpenter Hotel",
    summary: "Contemporary rooms in a hip hotel with a restaurant, a cafe & an outdoor pool, plus a terrace.",
    address: "400 Josephine St, Austin, TX 78704",
    hours: "24/7",
    price: "291 a night",
    ratings: "4.6/5",
    comment: "I absolutely loved my stay at the Carpenter Hotel! The room was spacious & clean! The bed and pillows made my day of traveling feel like a dream!",
    type: "Contemporary style hotel",
    image: "./model_images/line.png",
    image2: "./components/model_images/line2.png",
    longsummary: "Carpenter Hotel evokes an easy time in Austin full of fresh air, Texas sunshine, and days spent lazing by the water with a book. Our spacious, open-air hotel is in the center of it all: a short walk to the west grants access to the green expanses of Zilker Park, the legendary waters of Barton Springs, and the meandering Green Belt. Access to the Ann and Roy Butler Hike and Bike trail is just one block to the north before you cross Lady Bird Lake and explore Downtown Austin.",
    hotels: "None",
    restaurants: "None",
    spots: "Barton Springs",
    todo: "Free Wi-Fi,    Paid breakfast,    Paid parking,    Accessible,    Air-conditioned,    Business center",
    phone: "(512) 682-5300"        ,
    link: "https://carpenterhotel.com/",
    id: 3
},

]
