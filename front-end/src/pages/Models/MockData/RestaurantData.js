export const YelpApiData = [
    {
        id: 1,
        "media": "https://www.youtube.com/watch?v=omAoFAlxLvM&t=1s",
        "alias": "acre-41-austin",
        "name": "Acre 41",
        "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/CWQhN3dq8Kw-7v3F912jyA/o.jpg",
        "is_claimed": true,
        "is_closed": false,
        "url": "https://www.yelp.com/biz/acre-41-austin?adjust_creative=m-V1ZRRCB2LClSJvIcO_HQ&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_lookup&utm_source=m-V1ZRRCB2LClSJvIcO_HQ",
        "phone": "+17372439020",
        "display_phone": "(737) 243-9020",
        "review_count": 156,
        "categories": [
            {
                "alias": "newamerican",
                "title": "American (New)"
            },
            {
                "alias": "breakfast_brunch",
                "title": "Breakfast & Brunch"
            },
            {
                "alias": "steak",
                "title": "Steakhouses"
            }
        ],
        "rating": 4.0,
        "location": {
            "address1": "1901 San Antonio St",
            "address2": "",
            "address3": null,
            "city": "Austin",
            "zip_code": "78705",
            "country": "US",
            "state": "TX",
            "display_address": [
                "1901 San Antonio St",
                "Austin, TX 78705"
            ],
            "cross_streets": ""
        },
        "coordinates": {
            "latitude": 30.282339,
            "longitude": -97.74295049251636
        },
        "photos": [
            "https://s3-media3.fl.yelpcdn.com/bphoto/CWQhN3dq8Kw-7v3F912jyA/o.jpg",
            "https://s3-media2.fl.yelpcdn.com/bphoto/DqthD1E1evqNBnTVikb8nw/o.jpg",
            "https://s3-media1.fl.yelpcdn.com/bphoto/apNEBXlWC3ZjqnHVlVb2yw/o.jpg"
        ],
        "price": "$$$",
        "hours": [
            {
                "open": [
                    {
                        "is_overnight": false,
                        "start": "0700",
                        "end": "1400",
                        "day": 0
                    },
                    {
                        "is_overnight": false,
                        "start": "1700",
                        "end": "2200",
                        "day": 0
                    },
                    {
                        "is_overnight": false,
                        "start": "0700",
                        "end": "1400",
                        "day": 1
                    },
                    {
                        "is_overnight": false,
                        "start": "1700",
                        "end": "2200",
                        "day": 1
                    },
                    {
                        "is_overnight": false,
                        "start": "0700",
                        "end": "1400",
                        "day": 2
                    },
                    {
                        "is_overnight": false,
                        "start": "1700",
                        "end": "2200",
                        "day": 2
                    },
                    {
                        "is_overnight": false,
                        "start": "0700",
                        "end": "1400",
                        "day": 3
                    },
                    {
                        "is_overnight": false,
                        "start": "1700",
                        "end": "2200",
                        "day": 3
                    },
                    {
                        "is_overnight": false,
                        "start": "0700",
                        "end": "1400",
                        "day": 4
                    },
                    {
                        "is_overnight": false,
                        "start": "1700",
                        "end": "2200",
                        "day": 4
                    },
                    {
                        "is_overnight": false,
                        "start": "0700",
                        "end": "1400",
                        "day": 5
                    },
                    {
                        "is_overnight": false,
                        "start": "1700",
                        "end": "2200",
                        "day": 5
                    },
                    {
                        "is_overnight": false,
                        "start": "0700",
                        "end": "1400",
                        "day": 6
                    },
                    {
                        "is_overnight": false,
                        "start": "1700",
                        "end": "2200",
                        "day": 6
                    }
                ],
                "hours_type": "REGULAR",
                "is_open_now": true
            }
        ],
        "transactions": [
            "pickup"
        ],
        "messaging": {
            "url": "https://www.yelp.com/raq/cTwsI0U1x1xeOrCdgIaJ2w?adjust_creative=m-V1ZRRCB2LClSJvIcO_HQ&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_lookup&utm_source=m-V1ZRRCB2LClSJvIcO_HQ#popup%3Araq",
            "use_case_text": "Message the Business"
        }
    },
    

    {
        id: 2,
        "media": "https://www.youtube.com/watch?v=WcFD1yU27lw",
        "alias": "hestia-austin",
        "name": "Hestia",
        "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/z13YRmJGYKHpIcdFBTPG5g/o.jpg",
        "is_claimed": true,
        "is_closed": false,
        "url": "https://www.yelp.com/biz/hestia-austin?adjust_creative=m-V1ZRRCB2LClSJvIcO_HQ&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_lookup&utm_source=m-V1ZRRCB2LClSJvIcO_HQ",
        "phone": "+15123330737",
        "display_phone": "(512) 333-0737",
        "review_count": 229,
        "categories": [
            {
                "alias": "newamerican",
                "title": "American (New)"
            },
            {
                "alias": "seafood",
                "title": "Seafood"
            }
        ],
        "rating": 4.5,
        "location": {
            "address1": "607 West 3rd St",
            "address2": "Ste 105",
            "address3": null,
            "city": "Austin",
            "zip_code": "78701",
            "country": "US",
            "state": "TX",
            "display_address": [
                "607 West 3rd St",
                "Ste 105",
                "Austin, TX 78701"
            ],
            "cross_streets": ""
        },
        "coordinates": {
            "latitude": 30.266775,
            "longitude": -97.74998
        },
        "photos": [
            "https://s3-media2.fl.yelpcdn.com/bphoto/z13YRmJGYKHpIcdFBTPG5g/o.jpg",
            "https://s3-media4.fl.yelpcdn.com/bphoto/_3sxsa3eOyqv64X8Dc-6sA/o.jpg",
            "https://s3-media4.fl.yelpcdn.com/bphoto/nxRgRGs5k-hCCXuAjWIlLg/o.jpg"
        ],
        "price": "$$$",
        "hours": [
            {
                "open": [
                    {
                        "is_overnight": false,
                        "start": "1730",
                        "end": "2200",
                        "day": 1
                    },
                    {
                        "is_overnight": false,
                        "start": "1730",
                        "end": "2200",
                        "day": 2
                    },
                    {
                        "is_overnight": false,
                        "start": "1730",
                        "end": "2200",
                        "day": 3
                    },
                    {
                        "is_overnight": false,
                        "start": "1730",
                        "end": "2200",
                        "day": 4
                    },
                    {
                        "is_overnight": false,
                        "start": "1730",
                        "end": "2200",
                        "day": 5
                    },
                    {
                        "is_overnight": false,
                        "start": "1730",
                        "end": "2200",
                        "day": 6
                    }
                ],
                "hours_type": "REGULAR",
                "is_open_now": false
            }
        ],
        "transactions": [
            "delivery"
        ]
    },
    {
        id: 3,
        "media": "https://www.youtube.com/watch?v=OCQckPc2HjQ",
        "alias": "olamaie-austin",
        "name": "Olamaie",
        "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/rOINltGlTvDngESWFlHlQg/o.jpg",
        "is_claimed": true,
        "is_closed": false,
        "url": "https://www.yelp.com/biz/olamaie-austin?adjust_creative=m-V1ZRRCB2LClSJvIcO_HQ&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_lookup&utm_source=m-V1ZRRCB2LClSJvIcO_HQ",
        "phone": "+15124742796",
        "display_phone": "(512) 474-2796",
        "review_count": 524,
        "categories": [
            {
                "alias": "southern",
                "title": "Southern"
            },
            {
                "alias": "newamerican",
                "title": "American (New)"
            },
            {
                "alias": "cocktailbars",
                "title": "Cocktail Bars"
            }
        ],
        "rating": 4.0,
        "location": {
            "address1": "1610 San Antonio St",
            "address2": "",
            "address3": "",
            "city": "Austin",
            "zip_code": "78701",
            "country": "US",
            "state": "TX",
            "display_address": [
                "1610 San Antonio St",
                "Austin, TX 78701"
            ],
            "cross_streets": ""
        },
        "coordinates": {
            "latitude": 30.2799,
            "longitude": -97.74367
        },
        "photos": [
            "https://s3-media2.fl.yelpcdn.com/bphoto/rOINltGlTvDngESWFlHlQg/o.jpg",
            "https://s3-media1.fl.yelpcdn.com/bphoto/sGF2vG9LxggzTHeLqYjM9w/o.jpg",
            "https://s3-media3.fl.yelpcdn.com/bphoto/rotPT897RnOqvtdqS-ARvg/o.jpg"
        ],
        "price": "$$$",
        "hours": [
            {
                "open": [
                    {
                        "is_overnight": false,
                        "start": "1700",
                        "end": "2200",
                        "day": 0
                    },
                    {
                        "is_overnight": false,
                        "start": "1700",
                        "end": "2200",
                        "day": 2
                    },
                    {
                        "is_overnight": false,
                        "start": "1700",
                        "end": "2200",
                        "day": 3
                    },
                    {
                        "is_overnight": false,
                        "start": "1700",
                        "end": "2200",
                        "day": 4
                    },
                    {
                        "is_overnight": false,
                        "start": "1700",
                        "end": "2200",
                        "day": 5
                    },
                    {
                        "is_overnight": false,
                        "start": "1700",
                        "end": "2200",
                        "day": 6
                    }
                ],
                "hours_type": "REGULAR",
                "is_open_now": true
            }
        ],
        "transactions": [
            "delivery"
        ]
    }
    
]












export const MockRestaurantData = [
    {
        name: "Acre41",
        summary: "Clubby New American restaurant specializing in burgers & steaks, plus happy hours & weekend brunch.",
        address: "1901 San Antonio St Unit 130, Austin, TX 78705",
        hours: "7 AM–2 PM, 5–10 PM",
        price: "$$",
        ratings: "4.8/5",
        comment: "While visiting, we stopped by here for a quick lunch. The food was amazing and staff was very friendly. I really wish there was one of these back home. I would definitely recommend!",
        type: "American Food",
        image: "./model_images/acre41.jpg",
        image2: "./components/model_images/acre412.jpg",
        longsummary: "As the only modern, upscale neighborhood restaurant in Austin’s Campus District, Acre 41’s dressed-up American menu features classic dishes that go the extra acre with locally sourced meats and other comfort food favorites at the highest quality that you can enjoy time and time again.",
        hotels: "The Otis",
        restaurants: "None",
        spots: "UT Tower",
        todo: "Belly on porkchop ($35), Ribeye ($59), Truffle Spaghetti ($20)",
        phone: "(737) 243-9020",
        services : "Dine-in · Takeout · Delivery",
        link: "https://www.acre41austin.com/",
        id: 1
    },
    {
        name: "Hestia",
        summary: "Industrial-style restaurant offering creative wood-fired dishes, plus tasting menus & wine pairings..",
        address: "607 W 3rd St Suite 105, Austin, TX 78701",
        hours: "5:30 - 11pm",
        price: "$$",
        ratings: "4.5/5",
        comment: "New year means getting a \"taste\" for what's to come and one of the best places to do that is the ultra chic, yet very approachable Hestia in DT ATX.",
        type: "Industrial Food",
        image: "./model_images/hestia.jpg",
        image2: "./components/model_images/hestia2.jpg",
        longsummary: "GODDESS OF THE HEARTH Our dishes find inspiration from a custom 20-ft hearth that anchors the kitchen and dining room. The menu allows an opportunity to explore and showcase techniques that highlight the role of fire in food. We offer a la carte selections, as well as a curated multi-course tasting menu to fully immerse yourself in the Hestia experience. In addition to our food menu, Hestia boasts a robust wine program with over 250 selections and a curated wine pairing for the tasting menu.",
        hotels: "None",
        restaurants: "Olamaie",
        spots: "None",
        todo: "Scallops, Beef Tartare, Dry Aged Texas Wagyu",
        phone: "(512) 333-0737",
        services : "Dine-in · Curbside pickup · Delivery",
        link: "https://hestiaaustin.com/",
        id: 2
    },
    {
        name: "Olamaie",
        summary: "Southern fare is elevated with farm-fresh ingredients & modern plating in elegant refurbished home.",
        address: "1610 San Antonio St, Austin, TX 78701",
        hours: "5 - 10pm",
        price: "$$",
        ratings: "4.5/5",
        comment: "Truly delicious. One of my favorite places in Atx. Drinks are incredible. The biscuits are insanely good, no disappointment here.",
        type: "Southern Food",
        image: "./model_images/olamaie.jpg",
        image2: "./components/model_images/olamaie2.jpg",
        longsummary: "If you’ve only heard one thing about Olamaie, it probably has something to do with the biscuits—a not-very-well-kept secret menu item from the first few years Olamaie was in business. These days the biscuits are readily available on the menu, and they’re every bit as good as you might guess—warm, flaky, and probably made with more butter than we’re comfortable imagining. And they’re just the start to what will probably be one of the most impressive hours you’ve had in a while, packed full of modern and fine dining takes on Southern comfort food.",
        hotels: "None",
        restaurants: "Hestia",
        spots: "None",
        todo: "Strube Ranch Wagyu ($18), Dewberry Hills Farm Chicken ($29), Kil't White Button Mushrooms ($13)",
        phone: "(512) 474-2796",
        services : "Dine-in · Takeout · Delivery",
        link: "https://olamaieaustin.com/",
        id: 3
    },



]
