

//new api data from api call 
export const TouristAPIData = [
    {   
        id:1,
        image: "https://upload.wikimedia.org/wikipedia/commons/1/19/Austin_history_center_2006.jpg",
        image2: "./components/model_images/historyCenter2.png",
        "xid": "W328649859",
        "name": "Austin History Center",
        "address": {
          "city": "Austin",
          "road": "Guadalupe Street",
          "house": "Austin History Center",
          "state": "Texas",
          "county": "Travis County",
          "country": "United States of America",
          "postcode": "78701",
          "country_code": "us",
          "house_number": "810",
          "neighbourhood": "Downtown"
        },
        "rate": "3h",
        "osm": "way/328649859",
        "bbox": {
          "lon_min": -97.746217,
          "lon_max": -97.745565,
          "lat_min": 30.271547,
          "lat_max": 30.271883
        },
        "wikidata": "Q2872117",
        "kinds": "cultural, museums, interesting places, other museums",
        "url": "http://www.ci.austin.tx.us/library/ahc/",
        "sources": {
          "geometry": "osm",
          "attributes": [
            "osm",
            "wikidata"
          ]
        },
        "otm": "https://opentripmap.com/en/card/W328649859",
        "wikipedia": "https://en.wikipedia.org/wiki/Austin%20History%20Center",
        "preview": {
          "source": "https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Austin_history_center_2006.jpg/400px-Austin_history_center_2006.jpg",
          "height": 308,
          "width": 400
        },
        "wikipedia_extracts": {
          "title": "en:Austin History Center",
          "text": "The Austin History Center is the local history collection of the Austin Public Library and the city's historical archive.",
          "html": "<p>The <b>Austin History Center</b> is the local history collection of the Austin Public Library and the city's historical archive.</p>"
        },
        "point": {
          "lon": -97.74588775634766,
          "lat": 30.27171516418457
        },
        "media":"https://www.youtube.com/watch?v=m_77DevuAV0"
      },
      {
        id: 2,
        "xid": "N3391837793",
        image: "https://upload.wikimedia.org/wikipedia/commons/e/e6/Austin_Aquarium_Exterior_2021.jpg",
        image2: "./components/model_images/aquarium2.png",
        "name": "Austin Aquarium",
        "address": {
          "city": "Austin",
          "road": "Caldwell Drive",
          "state": "Texas",
          "county": "Williamson County",
          "suburb": "Jollyville",
          "country": "United States of America",
          "postcode": "78750",
          "country_code": "us",
          "house_number": "13509",
          "neighbourhood": "Woodland Village"
        },
        "rate": "2",
        "osm": "node/3391837793",
        "wikidata": "Q14710319",
        "kinds": "aquariums, urban environment, gardens and parks, cultural, museums, interesting places, zoos",
        "url": "http://www.austinaquarium.com/",
        "sources": {
          "geometry": "osm",
          "attributes": [
            "osm",
            "wikidata"
          ]
        }, "bbox": {
          "lon_min": -97.758246,
          "lon_max": -97.7567,
          "lat_min": 30.276043,
          "lat_max": 30.27762
        },
        "otm": "https://opentripmap.com/en/card/N3391837793",
        "wikipedia": "https://en.wikipedia.org/wiki/Austin%20Aquarium",
        "wikipedia_extracts": {
          "title": "en:Austin Aquarium",
          "text": "The Austin Aquarium is a for profit aquarium located in Austin, Texas, United States, that opened to the public December 12, 2013. It is houses dozens of different species of reptiles, exotic birds and marine animals."
        },
        "point": {
          "lon": -97.79228210449219,
          "lat": 30.4498348236084
        },
        "media":"https://www.youtube.com/watch?v=1UYLk5FwPJM"
      },
      {
        id:3,
        "xid": "W210648030",
        image: "https://dynamic-media-cdn.tripadvisor.com/media/photo-o/0e/d7/d2/43/west-austin-park.jpg?w=1200&h=-1&s=1",
        image2: "./components/model_images/austinPark2.png",
        "name": "West Austin Park",
        "address": {
          "city": "Austin",
          "road": "West 10th Street",
          "house": "West Austin Dog Park",
          "state": "Texas",
          "county": "Travis County",
          "country": "United States of America",
          "postcode": "78703",
          "country_code": "us",
          "house_number": "1317",
          "neighbourhood": "Clarksville"
        },
        "rate": "2",
        "osm": "way/210648030",
        "bbox": {
          "lon_min": -97.758246,
          "lon_max": -97.7567,
          "lat_min": 30.276043,
          "lat_max": 30.27762
        },
        "wikidata": "Q31581869",
        "kinds": "urban environment, gardens and parks, cultural, interesting places",
        "sources": {
          "geometry": "osm",
          "attributes": [
            "osm",
            "wikidata"
          ]
        },
        "otm": "https://opentripmap.com/en/card/W210648030",
        "wikipedia": "https://ce.wikipedia.org/wiki/%3AWest%20Austin%20Park",
        "wikipedia_extracts": {
          "title": "ceb:West Austin Park",
          "text": "A beautiful park in Travis County. Features a playground, pool, and a dogpark. Something for the whole family!",
          "html": "<p>Parke ang <b>West Austin Park</b> sa Tinipong Bansa. Ang West Austin Park nahimutang sa kondado sa Travis County ug estado sa Texas, sa habagatan-sidlakang bahin sa nasod, 2,100 km sa habagatan-kasadpan sa ulohang dakbayan Washington, D.C. 159 metros ibabaw sa dagat kahaboga ang nahimutangan sa West Austin Park.</p>"
        },
        "point": {
          "lon": -97.75740814208984,
          "lat": 30.276887893676758
        },
        "media":"https://www.youtube.com/watch?v=YA3WoaH49aw"
      }

]