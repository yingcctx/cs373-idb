// S keleton for displaying info on the instance page. Fills in info using method calls that will retrieve the actual data.
import "./Models.css";
import { useLocation } from "react-router-dom";
import { Card, Container, Button } from 'react-bootstrap';
import { useState, useEffect } from "react";
import { Link } from "react-router-dom"
import styled from "styled-components"
import axios from "axios";
import { FindAllNear } from "./components/findNear";

export default function Restaurant() {


  const location = useLocation()
  const id = location.pathname
  const id2 = id.split("/")[3]


  const LinksWrapper = styled.div`display: flex; justify-content: center; margin: 20px`;

  const baseUrl = `https://api.cityscoop.me/restaurants/id=${id2}`;
  const [rest, restSet] = useState([]);
  const [latlng, latlngSet] = useState([]);
  const [addr, addrSet] = useState([]);
  const [image, imageSet] = useState([]);
  const [flavor, flavorSet] = useState("No flavor provided.");
  const[service, serviceSet] = useState("No services provided.")
  useEffect(() => {
    axios.get(baseUrl).then((response) => {
      let res = response.data
      restSet(res);
      latlngSet(res.rest_latlng ? res.rest_latlng : "None")
      addrSet(res.rest_address ? res.rest_address : "None")
      flavorSet(res.rest_type)
      imageSet(res.rest_image_url ? res.rest_image_url : "None")
      serviceSet(res.rest_transactions ? rest.rest_transactions : "Information not available.")
    });
  }, [baseUrl, rest.rest_transactions]);
  if(!rest) return null;
  var relatedAttractions = FindAllNear(latlng[0],latlng[1],'attractions');
  var relatedHotels = FindAllNear(latlng[0],latlng[1],'hotels');
  if(relatedAttractions[0] === undefined|| relatedHotels[0] === undefined){
    relatedAttractions[1] = "None Around"
    relatedAttractions[3] = "None Around"
    relatedHotels[1] = "None Around"
    relatedHotels[3] = "None Around"
  }
  return (
    
<Container>
    <Card bg={'dark'} text={'white'} style={{ width: '80rem', height: '180rem' }}>
    <Card.Img variant="top" style={{height:`40rem`}} src={image} />
    <div style={{padding: "10px"}}></div>
    <Card.Text  style={{fontSize:'25px'}}>{"Find us on the map!"}</Card.Text>
    <div style={{padding: "10px"}}></div>
    <div  style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        }}>
<iframe
      width="600"
      height="450"
      loading="lazy"
      title="googlemaps"
      allowFullScreen
      referrerPolicy="no-referrer-when-downgrade"
      src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyDvaZSadN8H4Z-mtGCK-5BuwtmXFYGk268&q=${latlng[0]} ${latlng[1]}`}
    ></iframe>    
    </div>
    <Card.Body>
      <Card.Title style={{fontSize:'90px'}}>{ rest.rest_name}</Card.Title> 
      <Card.Subtitle style={{fontSize:'30px'}}>{"Address: " + addr}</Card.Subtitle>
      <div style={{padding: "10px"}}></div>
      <Card.Text  style={{fontSize:'25px'}}>{"Type of Food Served: " +  flavor}</Card.Text>
      <Card.Text  style={{fontSize:'25px'}}>{"Service Types: " +  service}</Card.Text>
      <Card.Text  style={{fontSize:'25px'}}>{"Has This Restaurant been Verified/Claimed on yelp by the owners? : " +  true}</Card.Text>
      <Card.Text  style={{fontSize:'25px'}}>{"Coordinates: latitude: " +  latlng[0] + " longitude: " + latlng[1]}</Card.Text>
      
      <Link to={rest.rest_website+ ""} target="_blank">
      <Button variant="primary" style={{fontSize:'10px'}}>Learn More</Button>
       </Link>
       <div style={{padding: "30px"}}></div>
       <Card.Title style={{fontSize:'30px'}}> Related Information</Card.Title>
      <Card.Text style={{fontSize:'20px'}}>{"Hotels nearby: "}</Card.Text>
      <LinksWrapper className="links-wrapper">
      <Link to={`https://www.cityscoop.me/hotels/hotel/${relatedHotels[0]}`} >{ relatedHotels[1]}</Link><p>Distance: {relatedAttractions[2]}</p>
      <div style={{padding: "10px"}}></div>
      <br />
      <Link to={`https://www.cityscoop.me/hotels/hotel/${relatedHotels[3]}`} >{relatedHotels[4]}</Link> <p>Distance: {relatedAttractions[2]}</p>
    </LinksWrapper>
      <Card.Text style={{fontSize:'20px'}}>{"Attractions nearby: "}</Card.Text>
      <LinksWrapper className="links-wrapper">
      <Link to={`https://www.cityscoop.me/attractions/spot/${relatedAttractions[0]}`} > {relatedAttractions[1]}</Link> <p>Distance: {relatedAttractions[2]}</p>
      <div style={{padding: "10px"}}></div>
      <Link to={`https://www.cityscoop.me/attractions/spot/${relatedAttractions[3]}`} > {relatedAttractions[4]} </Link> <p>Distance: {relatedAttractions[5]}</p>
    </LinksWrapper>
      
    </Card.Body>
  </Card>
  </Container>
  )
}
