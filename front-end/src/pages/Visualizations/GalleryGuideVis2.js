import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { BarChart, CartesianGrid, XAxis, Label, YAxis, ReferenceLine, Legend, Bar, ResponsiveContainer } from 'recharts';
import { useState, useEffect } from "react";
import axios from 'axios';

const FindArtistWithMostArtworks = () => {
    const baseUrl = `https://galleryguide.me/api/artists?sort=num_artworks+True&page=1`;
    const [artist, artistSet] = useState([]);
    var result = {}
    useEffect(() => {
        axios.get(baseUrl).then((response) => {
            const mappedArtwork = response.data.artists.map((artist) => ({
                ...artist,
                name: artist.name,
                num_artwork: Number(artist.num_artworks),
                }));
            artistSet(mappedArtwork);
        });
    }, [baseUrl]);

    if (!artist) return null;

    artist.forEach((arti, index) => {
        var artistName = arti.name
        if(arti.name === "Henri de Toulouse-Lautrec")
                    artistName = "Toulouse-Lautrec"
        var artistArtworks = arti.num_artwork
        result[artistName] = artistArtworks
    })

    return result
}


const GalleryGuideVis2 = () => {
    var artist_data = FindArtistWithMostArtworks()
    //var result = {}

    const graphData = []
    Object.keys(artist_data).forEach(artist => {
        graphData.push({ artist_name: artist, num_artworks: artist_data[artist] })
    })
    return (

        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Number of Artworks Published by Top Artists</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <BarChart
                            width={500}
                            height={500}
                            data={graphData}
                            margin={{
                                top: 5,
                                right: 20,
                                left: -20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="artist_name" type="category" style={{
                                 fontSize: '16px',
                                fontFamily: 'Times New Roman',}}/>

                            <YAxis tick={true} domain={[0, 1000]}>
                                <Label
                                    angle={-90}
                                    value="Number of Artworks Published"
                                    position="insideLeft"
                                    domain 
                                    dx={0}
                                    style={{ textAnchor: "middle" }} />
                            </YAxis>

                            <ReferenceLine y={0} stroke="#000" />
                            <Legend />
                            <Bar dataKey="num_artworks" name = "Number of Artworks" fill="#8884d8" />
                           
                        </BarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    );
}
export default GalleryGuideVis2;
