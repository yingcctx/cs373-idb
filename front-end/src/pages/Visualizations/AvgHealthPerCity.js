import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { RadialBarChart, RadialBar, Tooltip, Legend, ResponsiveContainer, Cell } from 'recharts';
import { useState, useEffect } from "react";
import axios from 'axios';
const COLORS = ["#EF9A9A", "#F48FB1", "#CE93D8","#8E44AD","#9FA8DA","#90CAF9", "#3498DB", "#80DEEA","#80CBC4", "#A5D6A7", "#7DCEA0", "#E6EE9C", "#FFF59D","#FFCC80", "#EC7063"];

const CustomTooltip = ({ active, payload, label }) => {
    if (active && payload && payload.length) {
        return (
            <div className="custom-tooltip" style={{ backgroundColor: '#f5f5f5', padding: '5px', border: '1px solid #ccc' }}>
                <p className="label">{`City: ${payload[0].payload.category}`}</p>
                <p className="intro">{`Average Health Rating: ${payload[0].value.toFixed(2)}`}</p>
            </div>
        );
    }

    return null;
};

const AvgHotHealthPerCity = () => {
    const baseUrl = `https://api.cityscoop.me/hotels`;
    const [rest, restSet] = useState([]);
    const [graphData, setGraphData] = useState([]);

    useEffect(() => {
        axios.get(baseUrl).then((response) => {
            restSet(response.data.Result);
        });
    }, [baseUrl]);

    useEffect(() => {
        if (rest) {
            var result = {}
            var num_cities = {}
            // ...
// ...
rest.forEach((spot, index) => {
    if (!spot["hot_S2C"] || spot["hot_S2C"].trim() === "") return; // Ignore hotels without hot_S2C

    var currCity = spot["hot_city"];
    var match = spot["hot_S2C"].match(/(\d+)\*/);
    if (!match) return; 

    var currHealth = parseInt(match[1]);

    if(currCity in num_cities) {
        num_cities[currCity] += 1;
    } else {
        num_cities[currCity] = 1;
    }

    if (currCity in result) {
        result[currCity] += currHealth;
    } else {
        result[currCity] = currHealth;
    }
});
// ...

 

            var data2 = [result, num_cities]
            var keys = Object.keys(data2[0])
            keys.forEach((city) => {
                data2[0][city] = data2[0][city] / data2[1][city]
            });

            const newGraphData = []
            Object.keys(data2[0]).forEach(city => {
                newGraphData.push({ category: city, avg_health: data2[0][city] })
            });
            setGraphData(newGraphData);
        }
    }, [rest]);

    if (!graphData || graphData.length === 0) return null;

    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Average Health Rating of Hotels Per City</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <RadialBarChart
                            width={800}
                            height={450}
                            cx="50%"
                            cy="50%"
                            innerRadius={25}
                            outerRadius="100%"
                            barSize={20}
                            data={graphData.sort((a, b) => a.avg_health - b.avg_health)}
                            startAngle={180}
                            endAngle={0}
                        >
                            <Tooltip content={<CustomTooltip />} />

                            <Legend
                                iconSize={10}
                                width={160}
                                height={140}
                                layout="vertical"
                                verticalAlign="middle"
                                wrapperStyle={{
                                    top: 0,
                                    left: 250,
                                    lineHeight: "24px",
                                }}
                                formatter={(value, entry, index) => {
                                    return <span>{graphData[index].category}</span>;
                                }}
                            />
                            <RadialBar
                                minAngle={15}
                                label={{ position: 'insideStart', fill: '#fff' }}
                                background
                                clockWise
                                dataKey="avg_health"
                            >
                                {graphData.map((entry, index) => (
                                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                                ))}
                            </RadialBar>
                        </RadialBarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    );
}

export default AvgHotHealthPerCity;
    