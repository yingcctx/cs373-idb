import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { BarChart, Bar, Label, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine, ResponsiveContainer } from 'recharts';
import { useState, useEffect } from "react";
import axios from 'axios';

export function FindPrice() {

    const baseUrl = `https://api.cityscoop.me/restaurants`;
    const [rest, restSet] = useState([]);
    var result = {}
    var num_cities = {}
    useEffect(() => {
        axios.get(baseUrl).then((response) => {
            restSet(response.data.Result);
        });
        // eslint-disable-next-line
    }, []);
    if (!rest) return null;

    rest.forEach((spot, index) => {
        var currCity = spot["rest_city"]
        var currPrice = 0
        if (spot["rest_pricing"].length <= 4) {
            currPrice = spot["rest_pricing"].length
            if(currCity in num_cities) {
                num_cities[currCity] += 1
            } else {
                num_cities[currCity] = 1
            }
        }

        if (currCity in result) {
            result[currCity] += currPrice
        }
        else {
            result[currCity] = currPrice
        }
    })

    return [result, num_cities]
}


const AvgRestCostPerCity = () => {
    var data2 = FindPrice()
    var result = {}

    var keys = Object.keys(data2[0])
    keys.forEach((city) => {
        data2[0][city] = data2[0][city] / data2[1][city]
    })

    keys.forEach((city) => {
        var numSigns = Math.ceil(data2[0][city])
        var sign = ""
        for (var i = 0; i < numSigns; i++) {
            sign += "$"
        }
        result[city] = sign
    })

    const graphData = []
    Object.keys(data2[0]).forEach(city => {
        graphData.push({ category: city, avg_price: data2[0][city] })
    })
    return (

        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Average Price of Restaurants Per City (2 = $$, 3 = $$$, 4 = $$$$)</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <BarChart
                            width={500}
                            height={300}
                            data={graphData}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="category" tick={false}>
                                <Label
                                    value="City Name"
                                    position="insideBottom"
                                    dy={0}
                                    style={{ textAnchor: "middle" }} />
                            </XAxis>

                            <YAxis tick={true} domain={[1.7, 2.6]}>
                                <Label
                                    angle={-90}
                                    value="Average Price Listing"
                                    position="insideLeft"
                                    domain 
                                    dx={0}
                                    style={{ textAnchor: "middle" }} />
                            </YAxis>

                            <ReferenceLine y={0} stroke="#000" />
                            <Tooltip />
                            <Legend />
                            <Bar dataKey="avg_price" fill="#8884d8" />
                           
                        </BarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    );
}

export default AvgRestCostPerCity;