import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { PieChart, Pie, Cell, Tooltip, ResponsiveContainer } from 'recharts';
import { useState, useEffect } from "react";
import axios from 'axios';

const COLORS = ["#EF9A9A", "#F48FB1", "#CE93D8","#8E44AD","#9FA8DA","#90CAF9", "#3498DB", "#80DEEA","#80CBC4", "#A5D6A7", "#7DCEA0", "#E6EE9C", "#FFF59D","#FFCC80", "#EC7063"];

export function FindNum() {

    const baseUrl = `https://api.cityscoop.me/attractions`;
    const [att, attSet] = useState([]);
    var result = {}
    useEffect(() => {
        axios.get(baseUrl).then((response) => {
            attSet(response.data.Result);
        });
        // eslint-disable-next-line
    }, []);
    if (!att) return null;

    att.forEach((spot) => {
        var currCity = spot.att_categories[0]

        var currPrice = 1

        if (currCity in result) {
            result[currCity] = Number(result[currCity]) + 1
        }
        else {
            result[currCity] = currPrice
        }
    })

    return result
}


const SpotsPerCategory = () => {
    var data2 = FindNum()

    const graphData = []
    Object.keys(data2).forEach(type => {
        console.log("key: " + type)
        console.log("num: " + data2[type])
        graphData.push({ name: type, count: data2[type] })
    })
    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Number of Attractions Per Attraction Type/Category</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <PieChart width={400} height={400}>
                            <Pie
                                dataKey="count"
                                isAnimationActive={false}
                                data={graphData}
                                cx="50%"
                                cy="50%"
                                outerRadius={200}
                                fill="#8884d8"
                                label
                            >
                            {graphData.map((entry, index) => (
                                <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                            ))}

                        </Pie>
                        <Tooltip />
                    </PieChart>
                </ResponsiveContainer>
            </Col>
        </Row>
        </Container >
    );
}

export default SpotsPerCategory;
