import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { PieChart, Pie, Cell, Tooltip, ResponsiveContainer } from 'recharts';
import { useState, useEffect } from "react";
import axios from 'axios';

const COLORS = ["#EF9A9A", "#F48FB1", "#CE93D8","#8E44AD","#9FA8DA","#90CAF9", "#3498DB", "#80DEEA","#80CBC4", "#A5D6A7", "#7DCEA0", "#E6EE9C", "#FFF59D","#FFCC80", "#EC7063"];

export function FindNumOfCategories(){

    const baseUrl = `https://galleryguide.me/api/all_artworks`;
    const [art, artSet] = useState([]);
    var result = {}
    useEffect(() => {
        axios.get(baseUrl).then((response) => {
            const mappedArtwork = response.data.artworks.map((artwork) => ({
            ...artwork,
            category: artwork.medium
            }));
            artSet(mappedArtwork);
        });
    }, [baseUrl]);
    if (!art) return null;

    art.forEach((artwork) => {
        var artCategory = artwork.medium

        if (artCategory in result) {
            result[artCategory] = Number(result[artCategory]) + 1
        }
        else {
            result[artCategory] = 1
        }
    })
    
    return result
}

const GalleryGuideVis1 = () => {
    var numOfArtCategories = FindNumOfCategories()

    const pieGraphData = []
    Object.keys(numOfArtCategories).forEach(art => {
        console.log("key: " + art)
        console.log("num: " + numOfArtCategories[art])
        if(numOfArtCategories[art] > 50)
            pieGraphData.push({ name: art, count: numOfArtCategories[art] })
    })
    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Number of Artworks Per Art Category</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <PieChart width={1000} height={1000}>
                            <Pie
                                dataKey="count"
                                isAnimationActive={false}
                                data={pieGraphData}
                                cx="50%"
                                cy="50%"
                                outerRadius={200}
                                fill="#8884d8"
                                label
                            >
                            {pieGraphData.map((entry, index) => (
                                <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                            ))}

                        </Pie>
                        <Tooltip />
                    </PieChart>
                </ResponsiveContainer>
            </Col>
        </Row>
        </Container >
    );
}

export default GalleryGuideVis1;
