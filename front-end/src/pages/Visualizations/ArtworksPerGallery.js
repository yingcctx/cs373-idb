import React, { useState, useEffect } from "react";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";

import axios from "axios";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

const CustomTooltip = ({ active, payload, label }) => {
  if (active && payload && payload.length) {
    return (
      <div className="custom-tooltip">
        <p className="label">{`Gallery: ${label}`}</p>
        <p className="artwork-count">{`Artwork Count: ${payload[0].value}`}</p>
      </div>
    );
  }
  return null;
};

const BarGraph = () => {
  const baseUrl = "https://galleryguide.me/api/all_galleries";
  const [galleries, setGalleries] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const [response1] = await Promise.all([
        axios.get(baseUrl)
      ]);

      const combinedData = response1.data.galleries;

      const processedData = combinedData.map((gallery) => ({
        ...gallery,
        num_artists: Number(gallery.num_artists),
        num_artworks: Number(gallery.num_artworks),
      }));

      // Sort the galleries by the number of artworks in ascending order
      const sortedData = processedData.sort((a, b) => a.num_artworks - b.num_artworks);

      setGalleries(sortedData);
    };

    fetchData();
  }, []);

  return (
    <Container fluid="md">
      <h3 className="p-5 text-center">Number of Artworks Per Gallery</h3>
      <Row className="d-flex justify-content-center align-items-center">
        <BarChart
          layout="vertical"
          width={800}
          height={800}
          data={galleries}
          margin={{
            top: 20,
            right: 20,
            bottom: 20,
            left: 20,
          }}
        >
          <CartesianGrid />
          <XAxis type="number" />
          <YAxis dataKey="name" type="category" width={150} />
          <Tooltip content={<CustomTooltip />} />
          <Legend />
          <Bar dataKey="num_artworks" name="Number of Artworks" fill="#82ca9d" barSize={20} />
        </BarChart>
      </Row>
    </Container>
  );
};

export default BarGraph;
