// Layout for the Home page of the website. Displays our 3 modules.
import { Card, Row, Container, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
import "./Home.css";
import Form from "react-bootstrap/Form";

const handleSubmit = (event) => {
  event.preventDefault()
  const form = event.currentTarget
  console.log(`search query: ${form.query.value}`)
  window.location.assign(`hotels/search/${form.query.value}`)
}

const HomeData = [
  {
    name: "Attractions",
    summary: "Explore popular tourist destinations!",
    image: './Models/components/model_images/attract.jpg',
    id: 1
  },
  {
    name: "Restaurants",
    summary: "Explore popular eats!",
    image:'./Models/components/model_images/rest.jpg',
    id: 2
  },
  {
    name: "Hotels",
    summary: "Explore popular lodging!",
    image: './Models/components/model_images/hotel.jpg',
    id: 3
  }
]


export default function Home() {
  return (

    
    <Container>

      
<Container className="justify-content-end">
            <Form onSubmit={handleSubmit} className="d-flex">
              <Form.Control
                style={{ width: "22vw" }}
                type="search"
                name="query"
                placeholder="Search attractions, restaurants, and hotels"
                className="me-2"
                aria-label="Search"
              />
              <Button type="submit" variant="outline-secondary" style={{color: '#3d405b'}}>Search</Button>
            </Form>
          </Container>

    <div class="splash">
      <h1 data-testid={`home-title`}>Visiting a new city? Plan your trip with CityScoop!</h1>
      <div>
      <div class="cards_format">
      <Row>
        {/* <Col md="auto"> */}
          <Card
            text={'black'}
            style={{ width: '27rem', height: '35rem'}}>
            <Card.Img  variant="top"  style={{ width: '25rem', height: '15rem' }}  src={require(HomeData[0].image + "")} />
            <Card.Body>
              <Card.Title>{HomeData[0].name}</Card.Title>
              <Card.Text>{HomeData[0].summary}</Card.Text>
              
              <Link to={`attractions`} ><Button variant="primary" >Learn More</Button></Link>
            </Card.Body>
          </Card>

          <Card
            text={'black'}
            style={{ width: '27rem', height: '35rem'}}>
            <Card.Img  variant="top" style={{ width: '25rem', height: '15rem' }} src={require(HomeData[1].image + "")} />
            <Card.Body>
              <Card.Title>{HomeData[1].name}</Card.Title>
              <Card.Text>{HomeData[1].summary}</Card.Text>
              <Link to={`restaurants`} ><Button variant="primary" >Learn More</Button></Link>
            </Card.Body>
          </Card>
          <Card
            text={'black'}
            style={{ width: '27rem', height: '35rem'}}>
            <Card.Img  variant="top" style={{ width: '25rem', height: '15rem' }} src={require(HomeData[2].image + "")} />
            <Card.Body>
              <Card.Title>{HomeData[2].name}</Card.Title>
              <Card.Text>{HomeData[2].summary}</Card.Text>
              <Link to={`hotels`} ><Button variant="primary" >Learn More</Button></Link>
            </Card.Body>
          </Card>
        {/* </Col> */}
        </Row>
        </div> 
      </div>
    </div>
    </Container>
  )
}
