import Container from "react-bootstrap/Container";


import AvgRestCostPerCity from "./Visualizations/AvgRestCostPerCity";
import SpotsPerCategory from "./Visualizations/SpotsPerCategory";
import AvgHotHealthPerCity from "./Visualizations/AvgHealthPerCity";
import Typography from "@mui/material/Typography";



export default function Visualizations() {
    return (


        <Container>
            <Typography
                variant="h2"
                sx={{ textAlign: "center" }}
                style={{
                    padding: "15px"
                }}>
                Visualizations
            </Typography>
            <AvgRestCostPerCity></AvgRestCostPerCity>
            <SpotsPerCategory></SpotsPerCategory>
            <AvgHotHealthPerCity></AvgHotHealthPerCity>
        </Container>
    )
}
