import Container from "react-bootstrap/Container";
import Typography from "@mui/material/Typography";
import BarGraph from "./Visualizations/ArtworksPerGallery";
import GalleryGuideVis1 from "./Visualizations/GalleryGuideVis1";
import GalleryGuideVis2 from "./Visualizations/GalleryGuideVis2";



export default function ProviderVisualizations() {
    return (
        <Container>
            <Typography
                variant="h2"
                sx={{ textAlign: "center" }}
                style={{
                    padding: "15px"
                }}>
                Provider Visualizations
            </Typography>
            <BarGraph></BarGraph>
            <GalleryGuideVis1></GalleryGuideVis1>
            <GalleryGuideVis2></GalleryGuideVis2>
        </Container>
    )
}