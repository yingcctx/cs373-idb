import React, { useState, useCallback } from 'react';
import axios from "axios";
import { Card, Row, Col, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const team = [
    {
        name: "Ying Su",
        issueName: "Ying Su",
        bio: "A junior studying CS and UT Austin who loves to draw in her free-time!",
        responsibilities: "Front End",
        gitId: "@yingcctx",
        commits: 0,
        issues: 0,
        tests: 10,
        image: './images/AshleyPhoto.jpg'
    },

    {
        name: "Jack Perkins",
        issueName: "Jack Perkins",
        bio: "Sophomore CS major that likes to play music",
        responsibilities: "Full Stack",
        gitId: "@jackp5150",
        commits: 0,
        issues: 0,
        tests: 10,
        image: './images/JackPhoto.png'
    },

    {
        name: "jacob.dittoe",
        issueName: "Jacob Dittoe",
        bio: "Sophomore CS major who enjoys videogames and metal",
        responsibilities: "Database, Backend, AWS",
        gitId: "@jacob.dittoe",
        commits: 0,
        issues: 0,
        tests: 10,
        image: './images/JacobPhoto.JPG'
    },

    {
        name: "MJ",
        issueName: "Marlowe S Johnson",
        bio: "Senior in Computer Science who enjoys reading manga and playing the bass",
        responsibilities: "Full Stack",
        gitId: "@msj879",
        commits: 0,
        issues: 0,
        tests: 10,
        image: './images/MarlowePhoto.jpg'
    }
]

const tools = [
    {
        name: "React",
        desc: "React is a JavaScript library for building user interfaces and was used as the front-end and the react-app was used a foundation",
        image: './images/react.jpg',
        link: "https://reactjs.org"
    },
    {
        name: "Bootstrap CSS",
        desc: "Bootstrap components such as Buttons, Cards, etc. were used",
        image: './images/react-bootstrap.png',
        link: "https://react-bootstrap.netlify.app"
    },
    {
        name: "Postman",
        desc: "Postman is an API platform that was used to get API data and documente on the site's API",
        image: './images/postman.png',
        link: "https://www.postman.com"
    },
    {
        name: "GitLab",
        desc: "GitLab is a web-based Git repository that was the base of operations for source control and collaborative software development",
        image: './images/gitlab.jpg',
        link: "https://about.gitlab.com"
    },
    {
        name: "Visual Studio Code",
        desc: "Visual Studio Code is the editor used for CityScoops",
        image: './images/vsc.jpg',
        link: "https://code.visualstudio.com"
    },
    {
        name: "Namecheap",
        desc: "Namecheap was used to obtain the cityscoops.me domain name",
        image: './images/namecheap.png',
        link: "https://www.namecheap.com"
    },
    {
        name: "AWS",
        desc: "Amazon Web Services was used to host our site",
        image: './images/aws.png',
        link: "https://aws.amazon.com/"
    },
    {
        name: "SQLAlchemy",
        desc: "Backend software used to store our data",
        image: './images/sqlalchemy.jpg',
        link: "https://www.sqlalchemy.org/"
    },
    {
        name: "Flask",
        desc: "Backend language used for database calls",
        image: './images/flask.png',
        link: "https://flask.palletsprojects.com/en/2.2.x/"
    },
    {
        name: "AWS Elastic Beanstalk",
        desc: "Allowed us to deploy application and measure vitals",
        image: './images/elastic.png',
        link: "https://aws.amazon.com/elasticbeanstalk/"
    },
    {
        name: "AWS RDS",
        desc: "Allows us to keep our database secure",
        image: './images/rds.png',
        link: "https://aws.amazon.com/rds/"
    },
    {
        name: "Python",
        desc: "Backend language used for database calls",
        image: './images/python.jpeg',
        link: "https://www.python.org/"
    },
    {
        name: "Docker",
        desc: "Package manager used for consistency",
        image: './images/docker.png',
        link: "https://www.docker.com/"
    },
    {
        name: "Marshmallow-SQLAlchemy",
        desc: "Backend software used to store our data",
        image: './images/sqlalchemy.jpg',
        link: "https://marshmallow-sqlalchemy.readthedocs.io/en/latest/"
    }




]

const data = [
    {
        name: "Hotel Beds API",
        desc: "API used to gather data on hotels, scraped manually",
        image: './images/hotelbeds.png',
        link: "https://developer.hotelbeds.com/documentation/hotels/"
    },
    {
        name: "Yelp API",
        desc: "API used to gather data on restaurants, scraped manually",
        image: './images/yelp.jpg',
        link: "https://docs.developer.yelp.com/docs/fusion-intro"
    },
    {
        name: "Google Maps API",
        desc: "API used to plot position of instances on a map",
        image: './images/maps.png',
        link: "https://developers.google.com/maps"
    },
    {
        name: "GeoAPIfy",
        desc: "API also used to gather data on tourist attractions, scrapped manually.",
        image: './images/geoapi.png',
        link: "https://www.geoapify.com/"
    }

]
//'https://awsmp-logos.s3.amazonaws.com/7ad38c2d-693d-43a8-a14f-17625e04ecf6/47a8bfb05e313e5023b4c0928deb65bd.png',
//This api was used we found out the first api didn't have the information we needed.

export default function About() {


    const [issuesData, setIssues] = useState([]);
    const [issuesTotal, setTotalIssues] = useState(0);
    const [commitData, setCommitData] = useState([{ name: "", commits: "" }]);

    
    React.useEffect(() => {
        getCommits()
        getIssues()
        getIssuesStatistics()
    }, []);
    // Helper functions to calculate individual totals for commits and issues
    const calcFieldTotal = useCallback((gitData, name, totaledProp) => {
        const filtered = gitData.filter(member => member.name === name)
        return filtered.reduce((accum, item) => accum + item[totaledProp], 0)
    }, []);
    const calcIssues = useCallback((gitData, name) => {
        const filteredOpen = gitData.filter(issue => issue.assignee.name === name)
        const filteredClosed = gitData.filter(issue => issue.assignee.name === name)
        return filteredOpen.length + filteredClosed.length
    }, []);
    // Helper functions to calculate team totals for commits and issues
    const geTotalCount = useCallback((gitData, field) => {
        if ('commits' === field)
            return gitData.reduce((accum, item) => accum + item[field], 0)
        if ('issues' === field)
            return gitData.length
    }, []);

    // Helper functions to dynamically retrieve Gitlab API Data
    const getIssues = async () => {
        await axios.get("https://gitlab.com/api/v4/projects/43263137/issues").then((response) => {
            setIssues(response.data);
        })
    }
    const getIssuesStatistics = async () => {
        await axios.get("https://gitlab.com/api/v4/projects/39730463/issues_statistics").then((response) => {
            console.log(response.data)
            setTotalIssues(response.data.statistics.counts.all);
        })
    }

    const getCommits = async () => {
        await axios.get("https://gitlab.com/api/v4/projects/43263137/repository/contributors").then((response) => {
            setCommitData(response.data);
        })
    }
    
    return (
        <Container>
            <h1>About Page</h1>

            <h4>CityScoop is a website to aid people in planning their next trip. It will provide information on hotels,
        restaurants, and tourist attractions in cities across the US. It will also provide connections between these places, encouraging visitors to see all that these cities has to offer.</h4>
            <body> <Link to={"https://gitlab.com/yingcctx/cs373-idb"}>Link to Our Gitlab Repo</Link></body>
            <div style={{ padding: "12px" }}></div>
            <h4>Meet the Team!</h4>
            <div style={{ padding: "12px" }}></div>

            <Row> 
            {
                team.map((member) => {
                    return (
                        <Col md="auto">
                            <Card
                                bg={'dark'}
                                text={'white'}
                                style={{ width: '19rem', height: '41rem' }}>
                                <Card.Img variant="top"  style={{ width: '18.9rem', height: '18.9rem' }} src={require(member.image + "")} />
                                <Card.Body>
                                    <Card.Title>{member.issueName}</Card.Title>
                                    <Card.Subtitle>{"Role: " + member.responsibilities}</Card.Subtitle>
                                    <Card.Text>{"Gitlab ID: " + member.gitId}</Card.Text>
                                    <Card.Text>{"About me: " + member.bio}</Card.Text>
                                    <Card.Text>{"# Commits: " + calcFieldTotal(commitData, member.name, 'commits')}
                                    </Card.Text>
                                    <Card.Text>{"# Issues: " + calcIssues(issuesData, member.issueName)}</Card.Text>
                                    <Card.Text>{"# Unit Tests: " + member.tests}</Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        )
                    })
            } 
            </Row>

            <div style={{ padding: "20px" }}>
                Total Number Commits: {geTotalCount(commitData, 'commits')}
            </div>
            <div style={{ padding: "20px" }}>
                Total number Issues: {issuesTotal}
            </div>
            <div style={{ padding: "20px" }}>
                Total Number Unit Tests: 40
            </div>
            <div style={{ padding: "20px" }}>
            <a href="https://documenter.getpostman.com/view/25778467/2s935vjz6q" target={'_blank'} rel="noreferrer" >Postman Documentation</a>
            </div>
            <h4>Tools Used</h4>
            <Row>
            {
                tools.map((tool) => {
                    return (
                        <Col md="auto">
                            <Card
                                bg={'dark'}
                                text={'white'}
                                style={{ width: '11rem', margin: '2rem', height: '28rem' }}>
                                <Card.Img variant="top" src={require(tool.image + "")} />
                                <Card.Body>
                                    <Card.Title>{tool.name}</Card.Title>
                                    <Card.Text>{tool.desc}</Card.Text>
                                    <Link to={tool.link + ""}>{tool.link}</Link>
                                </Card.Body>
                            </Card>
                        </Col>
                    )
                })
            }
            </Row>

            <div style={{ padding: "20px" }}></div>
            <h4>Data Used</h4>
            <Row>
            {
                data.map((data) => {
                    return (
                        <Col md="auto">
                            <Card bg={'dark'} text={'white'} style={{ width: '15rem', margin: '2rem', height: '30rem' }}>
                                {<Card.Img variant="top" src={require(data.image + "")} /> }
                                <Card.Body>
                                    <Card.Title>{data.name}</Card.Title>
                                    <Card.Text>{data.desc}</Card.Text>
                                    <Link to={data.link + ""}>{data.link}</Link>
                                </Card.Body>
                            </Card>
                        </Col>
                    )
                })
            }
            </Row>
        <h4>Disparate Data Usage: </h4>
        <p>We found that through our synthesization of three different data sources, CityScoop, as a tool
            is able to provide everything you need to travel to a new place. Whether it's cool restaurants to check
            out, a place to stay for the night, or play tourist for the day, CityScoop is able to equip you
            with the tools you need for a successful retreat away from the responsibilities of day-to-day life.
            While there is nothing particularly novel or interesting obtained from the combination of these sources,
            CityScoop is more meant to be a one-stop shop for planning vacations. </p>
        </Container>
    )
}