import { Link, useMatch, useResolvedPath } from "react-router-dom"
import logo from "./pages/images/cityScoopLogo.png"

export default function Navbar() {
  return (
    <nav className="nav">
      <img  alt="logo" style={{ width: '4rem', height: '4rem' }} src={logo}></img>
      <Link to="/" className="site-title">
        CityScoop
      </Link>
      <ul>
        <CustomLink to="/attractions"> Tourist Attractions</CustomLink>
        <CustomLink to="/restaurants">Restaurants</CustomLink>
        <CustomLink to="/hotels">Hotels</CustomLink>
        <CustomLink to="/visualizations">Visualizations</CustomLink>
        <CustomLink to="/providerVisualizations">Provider Visualizations</CustomLink>
        <CustomLink to="/about">About</CustomLink>
       
      </ul>
    </nav>
  )
}

function CustomLink({ to, children, ...props }) {
  const resolvedPath = useResolvedPath(to)
  const isActive = useMatch({ path: resolvedPath.pathname, end: true })

  return (
    <li className={isActive ? "active" : ""}>
      <Link to={to} {...props}>
        {children}
      </Link>
    </li>
  )
}
