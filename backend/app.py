from schemas import restaurants_schema, hotels_schema, attractions_schema
from models import Restaurants, Attractions, Hotels

from init import db, app
from flask import request, jsonify
from sqlalchemy import or_, desc


@app.route("/")
def home():
    return "Hello World!"


@app.route("/restaurants")
def get_restaurants():
    # Pagination
    page = request.args.get("page", type=int)
    perPage = request.args.get("perPage", type=int)
    # Sort
    sort = request.args.get("sort", type=str)
    order = request.args.get("ascending", type=str)
    # Filter
    minRating = request.args.get("minRating", type=float)
    maxRating = request.args.get("maxRating", type=float)
    minReviews = request.args.get("minReviews", type=int)
    maxReviews = request.args.get("maxReviews", type=int)
    price = request.args.get("prices", type=str)
    city = request.args.get("city", type=str)
    flavor = request.args.get("flavor", type=str)
    # Search
    search = request.args.get("search", type=str)

    query = db.session.query(Restaurants)

    # Filtering
    if minRating is not None:
        query = query.filter(Restaurants.rest_rating >= minRating)
    if maxRating is not None:
        query = query.filter(Restaurants.rest_rating <= maxRating)
    if minReviews is not None:
        query = query.filter(Restaurants.rest_review_count >= minReviews)
    if maxReviews is not None:
        query = query.filter(Restaurants.rest_review_count <= maxReviews)
    if price is not None:
        query = query.filter(Restaurants.rest_pricing == price)
    if city is not None and city != "":
        query = query.filter(Restaurants.rest_city.ilike(city))
    if flavor is not None:
        query = query.filter(Restaurants.rest_type.ilike(flavor))

    # Searching
    if search is not None:
        search = search.split(" ")
        results = []
        for key in search:
            formatted_key: str = "%" + key + "%"
            transactionKey = key.lower()
            results.append(Restaurants.rest_name.ilike(formatted_key))
            results.append(Restaurants.rest_transactions.contains([transactionKey]))
            results.append(Restaurants.rest_pricing.ilike(formatted_key))
            results.append(Restaurants.rest_type.ilike(formatted_key))
            results.append(Restaurants.rest_city.ilike(formatted_key))
        query = query.filter(or_(*results))

    # Sorting
    if sort is not None and sort != "sort" and getattr(Restaurants, sort) is not None:
        if order == "true":
            query = query.order_by(getattr(Restaurants, sort))
        else:
            query = query.order_by(desc(getattr(Restaurants, sort)))

    instances = query.count()
    if page is not None:
        query = paginate(query, page, perPage)
    result = restaurants_schema.dump(query, many=True)
    return jsonify({"Result": result, "Metadata": instances})


@app.route("/restaurants/id=<string:id>")
def get_restaurant_by_id(id):
    restaurant = db.session.query(Restaurants).filter_by(rest_id=id).first()
    result = restaurants_schema.dump(restaurant)
    return jsonify(result)


@app.route("/attractions")
def get_attractions():
    # Pagination
    page = request.args.get("page", type=int)
    perPage = request.args.get("perPage", type=int)
    # Sort
    sort = request.args.get("sort", type=str)
    order = request.args.get("ascending", type=str)
    # Filter
    city = request.args.get("city", type=str)
    state = request.args.get("state", type=str)
    type = request.args.get("type", type=str)
    attribute = request.args.get("attributes", type=str)
    hasHours = request.args.get("hasHours", type=str)
    # Search
    search = request.args.get("search", type=str)

    query = db.session.query(Attractions)

    # Filtering
    if city is not None and city != "":
        query = query.filter(Attractions.att_city.ilike(city))
    if state is not None:
        query = query.filter(Attractions.att_state.ilike(state))
    if type is not None:
        query = query.filter(Attractions.att_categories.any(type))
    if attribute is not None:
        query = query.filter(Attractions.att_categories.any(attribute))
    if hasHours is not None and hasHours == "True":
        query = query.filter(Attractions.att_hours != "No Hours Given")

    # Searching
    if search is not None and search != "":
        search = search.split(" ")
        results = []
        for key in search:
            formatted_key: str = "%" + key + "%"
            categoriesKey = key.lower()
            results.append(Attractions.att_name.ilike(formatted_key))
            results.append(Attractions.att_address.ilike(formatted_key))
            results.append(Attractions.att_state.ilike(formatted_key))
            results.append(Attractions.att_city.ilike(formatted_key))
            results.append(Attractions.att_categories.contains([categoriesKey]))
        query = query.filter(or_(*results))

    # Sorting
    if sort is not None and sort != "sort" and getattr(Attractions, sort) is not None:
        if order == "true":
            query = query.order_by(getattr(Attractions, sort))
        else:
            query = query.order_by(desc(getattr(Attractions, sort)))

    instances = query.count()
    if page is not None:
        query = paginate(query, page, perPage)
    result = attractions_schema.dump(query, many=True)
    return jsonify({"Result": result, "Metadata": instances})


@app.route("/attractions/id=<string:id>")
def get_attractions_by_id(id):
    attraction = db.session.query(Attractions).filter_by(att_id=id).first()
    result = attractions_schema.dump(attraction)
    return jsonify(result)


@app.route("/hotels")
def get_hotels():
    # Pagination
    page = request.args.get("page", type=int)
    perPage = request.args.get("perPage", type=int)
    # Sort
    sort = request.args.get("sort", type=str)
    order = request.args.get("ascending", type=str)
    # Filter
    starRating = request.args.get("starRating", type=str)
    healthRating = request.args.get("healthRating", type=str)
    city = request.args.get("city", type=str)
    hasWebsite = request.args.get("hasWebsite", type=bool)
    # Search
    search = request.args.get("search", type=str)

    query = db.session.query(Hotels)

    # Filtering
    if city is not None and city != "":
        query = query.filter(Hotels.hot_city.ilike(city))
    if starRating is not None:
        query = query.filter(Hotels.hot_category.ilike(starRating))
    if healthRating is not None:
        query = query.filter(Hotels.hot_S2C.ilike(healthRating))
    if hasWebsite is not None:
        query = query.filter(Hotels.hot_website != "No Website Listed")

    # Searching
    if search is not None:
        search = search.split(" ")
        results = []
        for key in search:
            formatted_key: str = "%" + key + "%"
            results.append(Hotels.hot_name.ilike(formatted_key))
            results.append(Hotels.hot_address.ilike(formatted_key))
            results.append(Hotels.hot_category.ilike(formatted_key))
            results.append(Hotels.hot_S2C.ilike(formatted_key))
            results.append(Hotels.hot_city.ilike(formatted_key))
        query = query.filter(or_(*results))

    # Sorting
    if sort is not None and sort != "sort" and getattr(Hotels, sort) is not None:
        if order == "true":
            query = query.order_by(getattr(Hotels, sort))
        else:
            query = query.order_by(desc(getattr(Hotels, sort)))

    instances = query.count()
    if page is not None:
        query = paginate(query, page, perPage)
    result = hotels_schema.dump(query, many=True)
    return jsonify({"Result": result, "Metadata": instances})


@app.route("/hotels/id=<string:id>")
def get_hotels_by_id(id):
    hotel = db.session.query(Hotels).filter_by(hot_id=id).first()
    result = hotels_schema.dump(hotel)
    return jsonify(result)


def paginate(query, page_num, perPage):
    if perPage is None:
        perPage = 10
    return query.paginate(page=page_num, per_page=perPage, error_out=False).items


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
