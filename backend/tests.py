import init
import app
import unittest
import json


class Tests(unittest.TestCase):
    def setUp(self):
        init.app.config["TESTING"] = True
        self.client = init.app.test_client()

    def testHome(self):
        with self.client:
            response = self.client.get("/")
            # Check that regular response status received
            self.assertEqual(response.status_code, 200)
            self.assertIsNotNone(response.data)

    def testGetAllHotels(self):
        with self.client:
            response = self.client.get("/hotels")
            # Check that regular response status received
            self.assertEqual(response.status_code, 200)
            self.assertIsNotNone(response.data)
            # Check that all 86 instances returned
            res_json = json.loads(response.data)
            self.assertEqual(res_json["Metadata"], 86)

    def testGetAllRestaurants(self):
        with self.client:
            response = self.client.get("/restaurants")
            # Check that regular response status received
            self.assertEqual(response.status_code, 200)
            self.assertIsNotNone(response.data)
            # Check that all 100 instances returned
            res_json = json.loads(response.data)
            self.assertEqual(res_json["Metadata"], 100)

    def testGetAllAttractions(self):
        with self.client:
            response = self.client.get("/attractions")
            # Check that regular response status received
            self.assertEqual(response.status_code, 200)
            self.assertIsNotNone(response.data)
            # Check that all 94 instances returned
            res_json = json.loads(response.data)
            self.assertEqual(res_json["Metadata"], 94)

    def testGetRestaurantByID(self):
        with self.client:
            response = self.client.get("/restaurants/id=1")
            # Check that regular response status received
            self.assertEqual(response.status_code, 200)
            self.assertIsNotNone(response.data)
            # Check that name of instance retrieved is as expected
            res_json = json.loads(response.data)
            self.assertEqual(res_json["rest_name"], "Qi Austin")

    def testGetHotelByID(self):
        with self.client:
            response = self.client.get("/hotels/id=1")
            # Check that regular response status received
            self.assertEqual(response.status_code, 200)
            self.assertIsNotNone(response.data)
            # Check that name of instance retrieved is as expected
            res_json = json.loads(response.data)
            self.assertEqual(res_json["hot_name"], "Omni Austin Hotel Downtown")

    def testGetAttractionByID(self):
        with self.client:
            response = self.client.get("/attractions/id=1")
            # Check that regular response status received
            self.assertEqual(response.status_code, 200)
            self.assertIsNotNone(response.data)
            # Check that name of instance retrieved is as expected
            res_json = json.loads(response.data)
            self.assertEqual(
                res_json["att_name"], "French Legation State Historic Site"
            )


if __name__ == "__main__":
    unittest.main()
