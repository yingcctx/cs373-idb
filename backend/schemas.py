from models import Restaurants, Attractions, Hotels
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field


class RestaurantsSchema(SQLAlchemySchema):
    class Meta:
        model = Restaurants
        ordered = True

    rest_id = auto_field()
    rest_name = auto_field()
    rest_type = auto_field()
    rest_rating = auto_field()
    rest_review_count = auto_field()
    rest_pricing = auto_field()
    rest_image_url = auto_field()
    rest_city = auto_field()
    rest_address = auto_field()
    rest_latlng = auto_field()
    rest_transactions = auto_field()
    rest_website = auto_field()


class AttractionsSchema(SQLAlchemySchema):
    class Meta:
        model = Attractions
        ordered = True

    att_id = auto_field()
    att_img = auto_field()
    att_name = auto_field()
    att_state = auto_field()
    att_city = auto_field()
    att_county = auto_field()
    att_address = auto_field()
    att_categories = auto_field()
    att_num_categories = auto_field()
    att_lon = auto_field()
    att_lat = auto_field()
    att_phone = auto_field()
    att_website = auto_field()
    att_hours = auto_field()


class HotelsSchema(SQLAlchemySchema):
    class Meta:
        model = Hotels
        ordered = True

    hot_id = auto_field()
    hot_name = auto_field()
    hot_desc = auto_field()
    hot_lon = auto_field()
    hot_lat = auto_field()
    hot_address = auto_field()
    hot_city = auto_field()
    hot_phone = auto_field()
    hot_email = auto_field()
    hot_image = auto_field()
    hot_website = auto_field()
    hot_S2C = auto_field()
    hot_category = auto_field()


restaurants_schema = RestaurantsSchema()
attractions_schema = AttractionsSchema()
hotels_schema = HotelsSchema()
