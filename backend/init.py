from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
app.debug = True


app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://{RDS_USERNAME}:{RDS_PASSWORD}@{RDS_HOSTNAME}:{RDS_PORT}/{RDS_DB_NAME}".format(
    RDS_USERNAME="postgres",
    RDS_PASSWORD="CityScoopGroup-4",
    RDS_HOSTNAME="database-1.c6viunaariys.us-east-2.rds.amazonaws.com",
    RDS_PORT=5432,
    RDS_DB_NAME="CityScoopDB",
)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)
