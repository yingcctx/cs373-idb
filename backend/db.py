import json
from init import app, db
from models import Restaurants, Hotels, Attractions

# GeoJobs (https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/back-end/db.py)
# and AustinEats (https://gitlab.com/mihikabirmiwal/cs373-idb/-/blob/develop/back-end/db_push.py)
# were very helpful for this part of the code, and our code is modeled after it.


def populate_db():
    populate_restaurants()
    populate_hotels()
    populate_attractions()


def populate_restaurants():
    with open("./API-Data/allcitiesyelp.json") as f:
        data = json.load(f)
        push_restaurants(data)
    db.session.commit()
    print("Restaurants added!")


def push_restaurants(data):
    id: int = 0
    for restaurant in data:
        new_rest = Restaurants(
            rest_id=id,
            rest_name=restaurant["name"],
            rest_type=restaurant["categories"][0]["title"],
            rest_rating=restaurant["rating"],
            rest_review_count=restaurant["review_count"],
            rest_pricing=restaurant["price"]
            if "price" in restaurant
            else " No Price Listed",
            rest_image_url=restaurant["image_url"],
            rest_city=restaurant["location"]["city"],
            rest_address=restaurant["location"]["display_address"],
            rest_latlng=[
                restaurant["coordinates"]["latitude"],
                restaurant["coordinates"]["longitude"],
            ],
            rest_transactions=restaurant["transactions"],
            rest_website=restaurant["url"],
        )
        db.session.add(new_rest)
        id += 1


def populate_hotels():
    with open("./API-Data/hotelData.json", encoding="utf-8") as f:
        data = json.load(f)
        push_hotels(data)
    db.session.commit()
    print("Hotels added!")


def push_hotels(data):
    id: int = 0
    for hotel in data:
        new_hotel = Hotels(
            hot_id=id,
            hot_name=hotel["name"]["content"],
            hot_desc=hotel["description"]["content"],
            hot_lon=hotel["coordinates"]["longitude"],
            hot_lat=hotel["coordinates"]["latitude"],
            hot_address=hotel["address"]["content"],
            hot_city=hotel["city"]["content"],
            hot_phone=hotel["phones"][0]["phoneNumber"],
            hot_email=hotel["email"] if "email" in hotel else "No Email Listed",
            hot_image=hotel["images"][0]["path"]
            if "images" in hotel
            else "No Image Given",
            hot_website=hotel["web"] if "web" in hotel else "No Website Listed",
            hot_S2C=hotel["S2C"] if "S2C" in hotel else " No Rating Listed",
            hot_category=hotel["categoryCode"],
        )
        db.session.add(new_hotel)
        id += 1


def populate_attractions():
    with open("./API-Data/attractions_data.json", encoding="utf-8") as f:
        data = json.load(f)
        push_attractions(data)
    db.session.commit()
    print("Attractions added!")


def push_attractions(data):
    id: int = 0
    for attraction in data:
        new_attraction = Attractions(
            att_id=id,
            att_img=attraction["img"],
            att_name=attraction["name"],
            att_state=attraction["state"],
            att_city=attraction["city"],
            att_county=attraction["county"]
            if "county" in attraction
            else "No County Listed",
            att_address=attraction["address_line2"],
            att_categories=attraction["categories"],
            att_num_categories=len(attraction["categories"]),
            att_lon=attraction["lon"],
            att_lat=attraction["lat"],
            att_phone=attraction["datasource"]["raw"]["phone"]
            if "phone" in attraction["datasource"]["raw"]
            else "No Phone Given",
            att_website=attraction["datasource"]["raw"]["website"]
            if "website" in attraction["datasource"]["raw"]
            else "No Website Given",
            att_hours=attraction["datasource"]["raw"]["opening_hours"]
            if "opening_hours" in attraction["datasource"]["raw"]
            else "No Hours Given",
        )
        db.session.add(new_attraction)
        id += 1


if __name__ == "__main__":
    with app.app_context():
        db.drop_all()
        db.create_all()
        populate_db()
