from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from init import db
from sqlalchemy.dialects.postgresql import ARRAY

# Based on models of GeoJobs (https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/back-end/models.py)


# Define restaurant table
class Restaurants(db.Model):
    __tablename__ = "Restaurants"

    rest_id = db.Column(db.Integer, primary_key=True)
    rest_name = db.Column(db.String)
    rest_type = db.Column(db.String)
    rest_rating = db.Column(db.Float)
    rest_review_count = db.Column(db.Integer)
    rest_pricing = db.Column(db.String)
    rest_image_url = db.Column(db.String)
    rest_city = db.Column(db.String)
    rest_address = db.Column(ARRAY(db.String()))
    rest_latlng = db.Column(ARRAY(db.Float()))
    rest_transactions = db.Column(ARRAY(db.String()))
    rest_website = db.Column(db.String)


class Attractions(db.Model):
    __tablename__ = "Attractions"

    att_id = db.Column(db.Integer, primary_key=True)
    att_img = db.Column(db.String)
    att_name = db.Column(db.String)
    att_state = db.Column(db.String)
    att_city = db.Column(db.String)
    att_county = db.Column(db.String)
    att_address = db.Column(db.String)
    att_categories = db.Column(ARRAY(db.String()))
    att_num_categories = db.Column(db.Integer)
    att_lon = db.Column(db.Float)
    att_lat = db.Column(db.Float)
    att_phone = db.Column(db.String)
    att_website = db.Column(db.String)
    att_hours = db.Column(db.String)


class Hotels(db.Model):
    __tablename__ = "Hotels"
    hot_id = db.Column(db.Integer, primary_key=True)
    hot_name = db.Column(db.String)
    hot_desc = db.Column(db.String)
    hot_lon = db.Column(db.Float)
    hot_lat = db.Column(db.Float)
    hot_address = db.Column(db.String)
    hot_city = db.Column(db.String)
    hot_phone = db.Column(db.String)
    hot_email = db.Column(db.String)
    hot_category = db.Column(db.String)
    hot_image = db.Column(db.String)
    hot_website = db.Column(db.String)
    hot_S2C = db.Column(db.String)
