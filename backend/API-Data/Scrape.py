# Business Search      URL -- 'https://api.yelp.com/v3/businesses/search'
# Business Match       URL -- 'https://api.yelp.com/v3/businesses/matches'
# Phone Search         URL -- 'https://api.yelp.com/v3/businesses/search/phone'

# Business Details     URL -- 'https://api.yelp.com/v3/businesses/{id}'
# Business Reviews     URL -- 'https://api.yelp.com/v3/businesses/{id}/reviews'

# Businesses, Total, Region

# Import the modules
import requests
import json

# Define a business ID
business_id = "4AErMBEoNzbk7Q8g45kKaQ"
unix_time = 1546047836

# Define my API Key, My Endpoint, and My Header
API_KEY = "ZE8yuF0oku6HSHZh0-Pf5CdW64hmKA6ZlYhFWgiFcM11K5dtduTU6_A-KVBtYnXnpeJNU_Q8puJeNWUScis3xA7shm6JYdEpqU2cvmtwrCyfqCKraxcB6T4ToenqY3Yx"
# ENDPOINT = 'https://api.yelp.com/v3/businesses/{}/reviews'.format(business_id)
ENDPOINT = "https://api.yelp.com/v3/businesses/search"
HEADERS = {"Authorization": "bearer %s" % API_KEY}

# Define my parameters of the search
# BUSINESS SEARCH PARAMETERS - EXAMPLE
PARAMETERS = {
    "term": "restaurants",
    "limit": 50,
    #  'offset': 50,
    "radius": 40000,
    "location": "Austin",
}


# Make a request to the Yelp API
response = requests.get(url=ENDPOINT, params=PARAMETERS, headers=HEADERS)

# Conver the JSON String
business_data = response.json()
# a


# print the response
# print(json.dumps(business_data, indent = 3))

# write results of call to file
f = open(".\\back-end\\API-Scraping\\yelp_results.json", "w")
f.write(json.dumps(business_data, indent=3))
f.close()
