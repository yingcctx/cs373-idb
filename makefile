.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

all:

# auto format the code
format:
	black ./backend/*.py

install:
	pip install -r front-end/tests/requirements.txt
	pip install -r backend/requirements.txt

backend-local: install
	python backend/app.py

update_database: install
	python backend/db.py

frontend-local: install
	cd front-end
	npm install
	npm start

frontend-production: install
	cd front-end
	npm run build

# check files, check their existence with make check
CFILES :=                         \
    .gitlab-ci.yml                        

# check the existence of check files
check: $(CFILES)

